import time
import requests
import json
import paho.mqtt.publish as publish
import os
from datetime import datetime

mqtt_broker = "ditto-mosquitto-1"
mqtt_port = 1883
mqtt_topic = "weather.sensor/01"

mqtt_username = os.getenv('mosquitto_username')
mqtt_password = os.getenv('mosquitto_password')





# Função para inserir dados na hiper-tabela
"""def insert_weather_data(weather_data):
    for data in weather_data:
        message = f'{{"temperature": {data["temperature"]}, "humidity": {data["humidity"]}, "dewPoint": {data["dew_point"]}, "windSpeed": {data["windSpeed"]}, "windDirection": {data["windDirection"]}, "cloudCover": {data["cloudCover"]}, "latitude": {data["latitude"]}, "longitude": {data["longitude"]}, "thingId": "weather.sensor:01"}}'
        publish.single(mqtt_topic, message, hostname=mqtt_broker, port=mqtt_port, auth={'username': mqtt_username, 'password': mqtt_password})
        time.sleep(0.1)"""

# Lê o arquivo JSON e extrai os dados relevantes
def read_and_prepare_data(file_path):
    with open(file_path, 'r') as file:
        data = json.load(file)
    weather_data = []
    for day in data['days']:
        date = datetime.strptime(day['datetime'], '%Y-%m-%d')
        for hour_data in day['hours']:
            # Primeiro, converter a string 'datetime' em um objeto datetime
            hour_time = datetime.strptime(hour_data['datetime'], '%H:%M:%S')  # ajuste este formato conforme necessário
            # Agora combinar a data com a hora
            weather_time = datetime(date.year, date.month, date.day, hour_time.hour, hour_time.minute)
            weather_data.append({
                'time': weather_time,
                'temperature': round((hour_data['temp'] - 32) * 5.0 / 9.0, 2),
                'humidity': hour_data['humidity'],
                'dew_point': round((hour_data['dew'] - 32) * 5.0 / 9.0, 2),
                'windSpeed': round(hour_data['windspeed'] * 1.60934, 2),
                'windDirection': hour_data['winddir'],
                'cloudCover': hour_data['cloudcover'],
                'latitude': data['latitude'],
                'longitude': data['longitude']
            })
    return weather_data

# Caminho para o arquivo JSON
file = 'data.json'

# Conexão com a base de dados

# Ler e preparar dados do JSON
weather_data = read_and_prepare_data(file)

# Inserir dados na hiper-tabela
#insert_weather_data(weather_data)  # Substitua 'your_hypertable_name' pelo nome da sua hiper-tabela




url = F"https://api.tomorrow.io/v4/weather/forecast?location=41.754784%2C%20-9.001056&apikey={os.getenv('API_TOKEN')}"
headers = {"accept": "application/json"}

def request():
    response = requests.get(url, headers=headers)
    print(response.text)
    weather = json.loads(response.text)['timelines']['minutely'][0]['values']

    temperature = weather['temperature']
    humidity = weather['humidity']
    dewPoint = weather['dewPoint']
    windSpeed = weather['windSpeed']
    windDirection = weather['windDirection']
    cloudCover = weather['cloudCover']
    location = json.loads(response.text)['location']
    latitude = location['lat']
    longitude = location['lon']


    message = f'{{"temperature": {temperature}, "humidity": {humidity}, "dewPoint": {dewPoint}, "windSpeed": {windSpeed}, "windDirection": {windDirection}, "cloudCover": {cloudCover}, "latitude": {latitude}, "longitude": {longitude}, "thingId": "weather.sensor:01"}}'

    publish.single(mqtt_topic, message, hostname=mqtt_broker, port=mqtt_port, auth={'username':mqtt_username, 'password':mqtt_password})

while True:
    request()
    time.sleep(180)