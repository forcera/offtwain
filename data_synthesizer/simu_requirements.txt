numpy==1.25.1
paho_mqtt==1.6.1
pandas==2.0.2
psycopg2==2.9.9
scikit_learn==1.3.2
scipy==1.11.4
matplotlib