import numpy as np
import pandas as pd
import os
import psycopg2
import warnings
warnings.filterwarnings('ignore')
from framework import offtwAIn_synthesizer

#Load .npy and save in the db
print('[DATA FEEDER] Processing the data...')
absolute_path = os.path.abspath('..')
file_model = './pre_proc_data/feed_model_data.npy'
file_synthetic = './pre_proc_data/feed_synthetic_data.npy'
model_array = np.load(os.path.join(file_model), allow_pickle=True)
synthetic_array = np.load(os.path.join(file_synthetic), allow_pickle=True)

#Save in a dictionary
#synthetic array
synthetic_data = [] #list to append the synthetic data dictionaries
for i in range(len(synthetic_array)):
    curr_entry = synthetic_array[i,:]
    synthetic_data.append({
        'time': curr_entry[0],
        'inclination': curr_entry[1],
        'irradiance': curr_entry[2],
        'i_OT00_dc': curr_entry[3],
        'v_OT00_dc': curr_entry[4],
        'fault_OT00': curr_entry[5],
        'i_OT01_dc': curr_entry[6],
        'v_OT01_dc': curr_entry[7],
        'fault_OT01': curr_entry[8],
        'i_OT02_dc': curr_entry[9],
        'v_OT02_dc': curr_entry[10],
        'fault_OT02': curr_entry[11],
        'i_OT03_dc': curr_entry[12],
        'v_OT03_dc': curr_entry[13],
        'fault_OT03': curr_entry[14],
        'i_OT04_dc': curr_entry[15],
        'v_OT04_dc': curr_entry[16],
        'fault_OT04': curr_entry[17],
        'i_OT05_dc': curr_entry[18],
        'v_OT05_dc': curr_entry[19],
        'fault_OT05': curr_entry[20],
        'i_OT06_dc': curr_entry[21],
        'v_OT06_dc': curr_entry[22],
        'fault_OT06': curr_entry[23],
        'i_OT07_dc': curr_entry[24],
        'v_OT07_dc': curr_entry[25],
        'fault_OT07': curr_entry[26],
        'p_OT0_dc': curr_entry[27],
        'status_OT0': curr_entry[28],
        'i_OT10_dc': curr_entry[29],
        'v_OT10_dc': curr_entry[30],
        'fault_OT10': curr_entry[31],
        'i_OT11_dc': curr_entry[32],
        'v_OT11_dc': curr_entry[33],
        'fault_OT11': curr_entry[34],
        'i_OT12_dc': curr_entry[35],
        'v_OT12_dc': curr_entry[36],
        'fault_OT12': curr_entry[37],
        'i_OT13_dc': curr_entry[38],
        'v_OT13_dc': curr_entry[39],
        'fault_OT13': curr_entry[40],
        'i_OT14_dc': curr_entry[41],
        'v_OT14_dc': curr_entry[42],
        'fault_OT14': curr_entry[43],
        'i_OT15_dc': curr_entry[44],
        'v_OT15_dc': curr_entry[45],
        'fault_OT15': curr_entry[46],
        'i_OT16_dc': curr_entry[47],
        'v_OT16_dc': curr_entry[48],
        'fault_OT16': curr_entry[49],
        'i_OT17_dc': curr_entry[50],
        'v_OT17_dc': curr_entry[51],
        'fault_OT17': curr_entry[52],
        'p_OT1_dc': curr_entry[53],
        'status_OT1': curr_entry[54],
        'i_OT20_dc': curr_entry[55],
        'v_OT20_dc': curr_entry[56],
        'fault_OT20': curr_entry[57],
        'i_OT21_dc': curr_entry[58],
        'v_OT21_dc': curr_entry[59],
        'fault_OT21': curr_entry[60],
        'i_OT22_dc': curr_entry[61],
        'v_OT22_dc': curr_entry[62],
        'fault_OT22': curr_entry[63],
        'i_OT23_dc': curr_entry[64],
        'v_OT23_dc': curr_entry[65],
        'fault_OT23': curr_entry[66],
        'i_OT24_dc': curr_entry[67],
        'v_OT24_dc': curr_entry[68],
        'fault_OT24': curr_entry[69],
        'i_OT25_dc': curr_entry[70],
        'v_OT25_dc': curr_entry[71],
        'fault_OT25': curr_entry[72],
        'i_OT26_dc': curr_entry[73],
        'v_OT26_dc': curr_entry[74],
        'fault_OT26': curr_entry[75],
        'i_OT27_dc': curr_entry[76],
        'v_OT27_dc': curr_entry[77],
        'fault_OT27': curr_entry[78],
        'p_OT2_dc': curr_entry[79],
        'status_OT2': curr_entry[80],
    })

#model array
model_data = [] #list to append the synthetic data dictionaries
for j in range(len(model_array)):
    curr_entry = model_array[j,:]
    model_data.append({
        'time': curr_entry[0],
        'mean_residue_OT0': curr_entry[1],
        'var_residue_OT0': curr_entry[2],
        'residue_OT0': curr_entry[3],
        'p_hat_OT0': curr_entry[4],
        'mean_residue_OT1': curr_entry[5],
        'var_residue_OT1': curr_entry[6],
        'residue_OT1': curr_entry[7],
        'p_hat_OT1': curr_entry[8],
        'mean_residue_OT2': curr_entry[9],
        'var_residue_OT2': curr_entry[10],
        'residue_OT2': curr_entry[11],
        'p_hat_OT2': curr_entry[12]
    })

#Covariances and theta arrays
# Control variables for the RLS algorithm
reg = 5  #number of regressors
theta = np.zeros((reg, 1))  #parameters vector
cov = 1000 * np.eye(len(theta)) #covariance matrix
cov_path = os.path.abspath('..')
np.save(os.path.join(f'./pre_proc_data/past_covariances/last_cov_OT0'), cov)
np.save(os.path.join(f'./pre_proc_data/past_thetas/last_theta_OT0'), theta)
np.save(os.path.join(f'./pre_proc_data/past_covariances/last_cov_OT1'), cov)
np.save(os.path.join(f'./pre_proc_data/past_thetas/last_theta_OT1'), theta)
np.save(os.path.join(f'./pre_proc_data/past_covariances/last_cov_OT2'), cov)
np.save(os.path.join(f'./pre_proc_data/past_thetas/last_theta_OT2'), theta)

#Feed the database
#Database information
database = 'your_database'
user = 'offtAin_timescaledb'
password = 'offtAin_timescaledb_password'
host = 'ditto-timescaledb-1'

conn = psycopg2.connect(
            host=host,
            port="5432",
            database=database,
            user=user,
            password=password)

#synthetic data
for k in synthetic_data:
    offtwAIn_synthesizer.insert_synthetic_data(conn, 'synthetic', k)
#model data
for l in model_data:
    offtwAIn_synthesizer.insert_model_data(conn, 'model_data', l)

#close comms
conn.close()

#Update files
abs_path = os.path.abspath('..')
flag_path = os.path.join('./pre_proc_data/last_update.txt')
time_path = os.path.join('./pre_proc_data/elapsed_time.txt')
resolution = 1
#update the last_update.txt file
"""with open(flag_path, 'w') as update:
    update.write(str(synthetic_data[-1]['time']))"""
#update the elapsed_time.txt file
with open(time_path, 'w') as elapsed:
    elapsed.write(str(float(len(synthetic_data*resolution))))
print('[DATA FEEDER] Data inserted in the database!')