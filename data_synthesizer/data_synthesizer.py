import psycopg2
import warnings
warnings.filterwarnings("ignore")
from framework import offtwAIn_synthesizer
import numpy as np
import os
#Database information
database = os.getenv('POSTGRES_DB')
user = os.getenv('POSTGRES_USER')
password = os.getenv('POSTGRES_PASSWORD')
host = 'ditto-timescaledb-1'

conn = psycopg2.connect(
            host=host,
            port="5432",
            database=database,
            user=user,
            password=password
        )

#MQTT broker information
broker_configs = {
        'host': 'ditto-mosquitto-1',
        'listening_topic': 'weather.sensor/01',
        'keepalive': 60,
        'mqtt_username': os.getenv('mosquitto_username'),
        'mqtt_password': os.getenv('mosquitto_password'),
        'status_topic': 'fault:',
        'general_topic': 'string:',
        'port': 1883
        }

#offtwAIn simulator arguments
altitude = 0 #altitude
Ns = 8 #cells in series
Np = 3 #strings

#offtwAIn simulator
weather_data = offtwAIn_synthesizer.read_weather(conn, demo=True) #complete weather data array
time_data = weather_data[:,0] #time of samples every hour
temp_data = weather_data[:,1] #temperature of samples every hour
wind_speed_data = weather_data[:,4] #wind speed of samples every hour
cloud_cover_data = weather_data[:,6] #cloud cover ratio of the location
latitude = weather_data[0,7] #latitude of the location
synthetic_data, model_data = offtwAIn_synthesizer.run_simu(time_data, temp_data, wind_speed_data, cloud_cover_data,
                                                           latitude, altitude, Ns, Np, broker_configs, conn)
                                                           

print('[offtwAIn_synthesizer] Data simulation complete!')
offtwAIn_synthesizer.read_synthetic(conn, 'synthetic', save=True)
offtwAIn_synthesizer.read_synthetic(conn, 'model_data', save=True)

#close comms
conn.commit()
conn.close()
