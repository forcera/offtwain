#!/bin/bash

# Start the first process
sleep 10
python3 database_feeder.py
python3 data_synthesizer.py

# Wait for any process to exit
wait -n

# Exit with status of process that exited first
exit $?