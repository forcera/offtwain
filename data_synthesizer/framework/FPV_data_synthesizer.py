import numpy as np
import os
import matplotlib
matplotlib.use('TkAgg') #pycharm bug fix
import matplotlib.pyplot as plt
import pandas as pd
from sklearn.metrics import mean_squared_error
from models import sky_models, simulation_models

class offtwAIn_synthesis:
    def __init__(self, latitude):
        '''
        :param latitude: latitude of the area where the pannel(s) is(are) located
        '''

        self.absolute_path = os.path.abspath('..')
        self.latitude = latitude
        self.v_dc1 = np.array([]) #DC voltage of the String 1
        self.i_dc1 = np.array([]) #DC current of the String 2
        self.v_dc2 = np.array([]) #DC voltage of the String 1
        self.i_dc2 = np.array([]) #DC current of the String 2
        self.irr = np.array([]) #Irradiance
        self.temp = np.array([]) #temperature
        self.timescale_data = np.array([]) #timescale weather data
        self.date_info = np.array([]) #time data from timescale
        self.exp_model_irr = np.array([]) #modeled irradiance (exponential model)
        self.vD_model_irr = np.array([]) #modeled irradiance (van Dam model)
        self.app_temp = np.array([]) #apparent temperature (cooling effect)
        #self.time_interval = np.arange(0, 23.8505, (1/3600)) #time steps for a day of acquisitions (1 Hz of sample rate)
        self.time_interval = np.arange(0, 24, 1) #time steps for a day of acquisitions (0.28 mHz of sample rate)
        self.wave_properties = np.array([]) #array containing wave properties for a given wind speed
        self.wind_speed = np.array([]) #array containing the wind speed data every second
        self.wave_period = np.array([]) #array containing the wave period data every second
        self.wave_height = np.array([]) #array containing the wave height data every second
        self.wave_pm = np.array([]) #wave spectrum computed with the pierson-malkowitz equation
        self.wave_b = np.array([]) #wave spectrum computed with the bretschneider equation
        self.FPV_slope = np.array([]) #inclination angles computed every second
        self.sloped_irr = np.array([]) #irradiances calculated from the inclinations
        self.p_dc_1 = list()  # DC Power output of the string 1
        self.p_dc_2 = list()  # DC Power output of the string 2
        self.system_output = list()
        self.emp_p_dc_1 = list()  # DC Power output of the string 1
        self.emp_p_dc_2 = list()  # DC Power output of the string 2
        self.sloped_p_dc_1 = list()  # DC Power output of the string 1
        self.sloped_p_dc_2 = list()  # DC Power output of the string 2
        self.sloped_system_output = list()

    def read(self, filter_acq=True, filter_day=None):
        '''
        :param n: debug variable to access specific dates in the PV dataset
        :param filter_acq: filter acquisitions to sample period of 1 hour inStead of 1s
        :param filter_day: [debug argument] every day in the dataset account for 85862 samples
        :return: eletrical and environmental variables from the PV dataset
        '''
        self.v_dc1, self.i_dc1, self.v_dc2, self.i_dc2, self.irr, self.temp = simulation_models.read_data()

        if filter_day is not None:
            n_init = filter_day * 85862
            n_end = n_init + 85862
            self.v_dc1 = self.v_dc1[n_init:n_end]
            self.i_dc1 = self.i_dc1[n_init:n_end]
            self.v_dc2 = self.v_dc2[n_init:n_end]
            self.i_dc2 = self.i_dc2[n_init:n_end]
            self.irr = self.irr[n_init:n_end]
            self.temp = self.temp[n_init:n_end]

        if filter_acq:
            hour_occ = 3600 #every 3600 elements is one hour (60[min]*60[s])
            self.v_dc1 = self.v_dc1[::hour_occ]
            self.i_dc1 = self.i_dc1[::hour_occ]
            self.v_dc2 = self.v_dc2[::hour_occ]
            self.i_dc2 = self.i_dc2[::hour_occ]
            self.irr = self.irr[::hour_occ]
            self.temp = self.temp[::hour_occ]

    def irr_compare(self):
        julian_day = sky_models.julian_day(12, 8)  # Aug. 12 in julian date
        self.exp_model_irr = np.zeros_like(self.irr)
        self.vD_model_irr = np.zeros_like(self.irr)
        for i in range(len(self.irr)):
            self.exp_model_irr[i] = simulation_models.exp_irr_model(self.latitude, julian_day, self.time_interval[i])
            self.vD_model_irr[i] = simulation_models.vanDam_irr_model(self.latitude, 30, 0.6,
                                                                      julian_day, self.time_interval[i], 935)
        print(mean_squared_error(self.irr, self.exp_model_irr))
        print(mean_squared_error(self.irr, self.vD_model_irr))
        plt.figure(5)
        leg = list()
        plt.plot(self.time_interval, self.irr)
        leg.append('Measured')
        plt.plot(self.time_interval, self.exp_model_irr)
        leg.append('Exponential model')
        plt.plot(self.time_interval, self.vD_model_irr)
        leg.append('van Dam model')
        plt.xlabel('Time')
        plt.ylabel('Irradiance [W/m^2]')
        plt.title('Irradiance Models vs. Measurements')
        plt.legend(leg)

    def gaussian(self, x, mu, sigma):
        '''
        :param x: range values
        :param mu: mean of the gaussian
        :param sigma: standart deviation of the gaussian
        :return: gaussian
        '''
        return np.exp(-((x - mu) ** 2) / (2 * (sigma ** 2)))

    def wave_gaussian(self, wave_length):
        '''
        :param wave_length: number of samples
        :return: a gaussian distribution for the effect on the angles
        '''
        mean = wave_length//2
        std_dev = wave_length/4
        x = np.linspace(0, wave_length - 1, wave_length)
        gaussian_output = self.gaussian(x, mean, std_dev)
        normalized_output = gaussian_output/np.max(gaussian_output) #setting max value to 1
        return normalized_output

    def load_wave_properties(self):
        load_path = os.path.join(self.absolute_path, 'data/wave_properties.npy')
        self.wave_properties = np.load(load_path)

    def load_timescale(self):
        read_path = os.path.join(self.absolute_path, 'ditto/pre_proc_data/weather_data.npy')
        data = np.load(read_path, allow_pickle=True)
        cols = ['time', 'temperature', 'humidity', 'dew_point', 'windSpeed', 'windDirection',
                'cloudCover', 'latitude', 'longitude']
        self.timescale_data = pd.DataFrame(data)
        self.timescale_data.columns = cols
        self.date_info = self.timescale_data['time']

    def pierson_moskowitz(self, H_s):
        freq_interval = np.linspace(0.001, 3, 1000)
        omega = 2*np.pi*freq_interval
        g = 9.81
        first_half = (0.0081*g**2)
        second_half = -0.032*((g/(H_s*omega**2))**2)
        return (first_half*omega**(-5))*np.exp(second_half), freq_interval

    def bretschneider_spectrum(self, H_s):
        freq_interval = np.linspace(0.001, 3, 1000)
        omega = 2 * np.pi * freq_interval
        g = 9.81
        omega_m = 0.4 * np.sqrt(g/H_s)
        first_half = 0.3125*((omega_m**4)/(omega**5))
        second_half = -1.25*(omega_m/omega)**4
        return first_half*H_s*np.exp(second_half), freq_interval

    def ocean_wave(self, plot=False, normalize=False):
        '''
        :param plot: plot the perfomance of the wave simulator compared to the acquired irradiance data
        :param normalize: normalize the spectrum with respect to the integral (trapezoidal method)
        :return: array with irradiance values accounting for waves at the specified period
        '''

        self.load_wave_properties()
        self.load_timescale()

        #Test date: Nov. 13th
        test_day = 13
        test_month = 11
        month_mask = np.where(self.date_info.dt.month == test_month)[0]
        self.date_info = self.date_info[month_mask]
        day_mask = self.date_info.index[0] + np.where(self.date_info.dt.day == test_day)[0]
        self.date_info = self.date_info[day_mask]

        self.wind_speed = self.timescale_data['windSpeed'].values.astype(float) #wind speed every hour
        self.wind_speed = self.wind_speed[self.date_info.index]
        self.wave_period = np.zeros_like(self.irr)
        self.wave_height = np.zeros_like(self.irr)
        for i in range(len(self.irr)):
            wave_idx = np.argmin(np.abs(self.wind_speed[i] - self.wave_properties[:,0]))
            self.wave_period[i] = self.wave_properties[wave_idx,1]
            self.wave_height[i] = self.wave_properties[wave_idx, 2]

        #H_s = 4*np.std(self.wave_height) #significant height (4*std_dev)
        #self.wave_pm, freq_pm = self.pierson_moskowitz(H_s) #wave spectrum using the pierson-moskowitz algorithm
        #self.wave_b, freq_b = self.bretschneider_spectrum(H_s) #wave spectrum using the bretschneider algorithm

        #if normalize:
            #normalizing the spectrum
            #pm_integral = np.trapz(self.wave_pm, freq_pm)
            #self.wave_pm /= pm_integral*H_s**2
        #    b_integral = np.trapz(self.wave_b, freq_b)
        #    self.wave_b /= b_integral*H_s**2

        #if plot:
        #    plt.figure(1)
        #    leg = list()
            #plt.plot(freq_pm/(2*np.pi), self.wave_pm)
            #leg.append('Pierson-Moskowitz')
        #    plt.plot(freq_b/(2*np.pi), self.wave_b, linestyle='dotted')
        #    leg.append('Bretschneider')
        #    plt.title('Wave Spectrum')
        #    plt.xlabel('Angular frequency (Hz)')
        #    plt.ylabel('Elevation [m^2/Hz]')
        #    plt.legend(leg)
        #    plt.show()

    def FPV_inclination(self, plot=False):
        #computing the inclinations
        self.FPV_slope = np.zeros_like(self.irr)
        for i in range(len(self.irr)):
            self.FPV_slope[i] = (self.wave_height[i] * np.sin(self.wave_period[i]) +
                                 self.wind_speed[i] * np.cos(self.wave_period[i])) * np.sin(self.wave_period[i])

        #computing the new irradiance vlaues from the inclination
        colour_coeff = 0.23  # waves, 1 inch or more with occasional whitecaps
        rough_coeff = 1.25  # waves, 1 inch or more with occasional whitecaps
        julian_day = sky_models.julian_day(13, 11)  # Nov. 13 in julian date
        self.sloped_irr = np.zeros_like(self.irr) #inclined irradiation values
        for j in range(len(self.irr)):
            curr_albedo = sky_models.albedo(colour_coeff, rough_coeff, julian_day, self.time_interval[j], self.latitude)
            I_dir, I_diff, I_gnd = sky_models.perez(13, 11,
                                                  self.time_interval[j], self.time_interval[j]+0.99,
                                                  np.abs(self.FPV_slope[j]), 0, self.latitude, curr_albedo,
                                                  self.vD_model_irr[j][0])
            I_total = np.sum([I_dir, I_diff, I_gnd]) #total irradiance using the anisotropic sky model
            #(0.999/3600) 1Hz
            if I_total < 0:
                I_total = 0  # accounting for possible negative numbers (irradiance shouldn't be smaller than 0)

            self.sloped_irr[j] = I_total

        if plot:
            plt.figure(2)
            plt.plot(self.time_interval, self.FPV_slope)
            plt.xlabel('Time [hr]')
            plt.ylabel('Inclination angle [°]')
            plt.title('FPV pannel inclination throughout the day')
            plt.show()

            plt.figure(3)
            leg = list()
            plt.plot(self.time_interval, self.vD_model_irr)
            leg.append('Model irradiance')
            plt.plot(self.time_interval, self.sloped_irr)
            leg.append('Slope simuated irradiance')
            plt.legend(leg)
            plt.xlabel('Time')
            plt.ylabel('Irradiance [W/m^2]')
            plt.title('Simulating inclination due to waves')
            plt.show()

    def cooling_effect(self, R_h, D=None):
        '''
        :param R_h: relative humidity (0-1 percentage)
        :param D: dew point (degrees celsius)
        :return: apparent temperature (degrees celsius)
        '''
        self.app_temp = np.zeros_like(self.temp)
        for i in range(len(self.temp)):
            if D is None:
                D = self.temp[i] - ((100 - R_h)/5)
            p_v = np.exp(1.8096 + ((17.69*D)/(273.3+D)))
            self.app_temp[i] = self.temp[i] + 0.33*p_v - 0.7*self.wind_speed[i] - 4

    def power_output_RLS(self, plot=False):
        '''
        :param plot: plot the perfomance of the wave simulator compared to the acquired output data
        :return: list with the power output accounting for waves at the specified period using the RLS algorithm
        '''

        for i in range(len(self.temp)):
            self.emp_p_dc_1.append(0.1697*15.52*self.sloped_irr[i]*(1-0.0041*(self.temp[i]-25)))
            self.emp_p_dc_2.append(0.1697*15.52*self.irr[i]*(1-0.0041*(self.temp[i]-25)))

        self.sloped_p_dc_1 = simulation_models.RLS_p(self.emp_p_dc_1, self.sloped_irr)
        self.sloped_p_dc_2 = simulation_models.RLS_p(self.emp_p_dc_2, self.sloped_irr)
        self.sloped_system_output = [sum(p_dc_out) for p_dc_out in zip(self.sloped_p_dc_1, self.sloped_p_dc_2)]

        if plot:
            plt.figure(4)
            #fig, axs = plt.subplots(1)
            #fig.suptitle('RLS results')

            leg = list()
            plt.plot(self.time_interval, self.v_dc1 * self.i_dc1)
            leg.append('Real output String 1')
            plt.plot(self.time_interval, self.emp_p_dc_2)
            leg.append('RLS String 1')
            plt.xlabel('Time')
            plt.ylabel('Power output [W]')
            plt.title('Power output comparison')
            plt.legend(leg)
            plt.grid()
            '''
            leg = list()
            axs[1].plot(self.time_interval, self.sloped_p_dc_2)
            leg.append('RLS String 2')
            axs[1].plot(self.time_interval, self.v_dc2 * self.i_dc2)
            leg.append('Measured on-shore data')
            axs[1].legend(leg)
            axs[1].grid()

            leg = list()
            axs[2].plot(self.time_interval, self.sloped_system_output)
            leg.append('RLS system')
            axs[2].plot(self.time_interval, self.v_dc1*self.i_dc1 + self.v_dc2 * self.i_dc2)
            leg.append('Measured on-shore data')
            axs[2].legend(leg)
            axs[2].grid()
            '''
    def run_simu(self):
        self.read(filter_day=5)
        self.irr_compare()
        self.ocean_wave()
        self.FPV_inclination(plot=True)
        self.power_output_RLS(plot=True)









