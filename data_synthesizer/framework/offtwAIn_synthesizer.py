import numpy as np
import os
import psycopg2
import paho.mqtt.client as mqtt
import time
import json
from psycopg2 import sql
from models import sky_models, simulation_models
from datetime import datetime, timezone, timedelta

def load_wave_properties():
    file = './pre_proc_data/wave_properties.npy'
    load_path = os.path.join(file)
    return np.load(load_path)

def ocean_wave(wind_speed, wave_properties):
    '''
    :param wind_speed: registered wind speed from the weather database
    :param wave_properties: interpolation of wave properties data from S.Z. Golroodbari and W. van Sark., 2020
    :return: FPV pannel inclination due to ocean activity
    '''
    wave_idx = np.argmin(np.abs(wind_speed - wave_properties[:,0])) #index of the wind_speed argument
    wave_period = wave_properties[wave_idx, 1] #wave period from wind speed
    wave_height = wave_properties[wave_idx, 2] #wave height from wind speed
    slope = (wave_height * np.sin(wave_period) +
             wind_speed * np.cos(wave_period)) * np.sin(wave_period) #angle of inclination due to ocean activity
    return slope

def FPV_irradiance(time_data, latitude, altitude, color_coeff, rough_coeff, slope, cc_ratio):
    '''
    :param time_data: timescale component of the current samples
    :param latitude: latitude of the pannel simulated
    :param altitude: altitude of the pannel simulated
    :param color_coeff: color coefficient for albedo computation from B.B. Hannabas., 1989
    :param rough_coeff: roughness coefficient for albedo computation from B.B. Hannabas., 1989
    :param slope: inclination of the FPV pannel
    :param cc_ratio: cloud cover ratio for the current sample
    :return: irradiances (standart and sloped) in [W/m^s]
    '''
    julian_day = sky_models.julian_day(time_data.day, time_data.month) #day of the year (N out of 365)
    time_instant = time_data.hour + (time_data.minute/60) + (time_data.second/3600) #time instant in decimals
    std_irradiance = simulation_models.vanDam_irr_model(latitude, 30, 0.6, julian_day, time_instant,
                                                        altitude, cc_ratio) #solar irradiance for the non-inclined pannel
    azimuth = sky_models.azimuth(latitude, julian_day, time_instant) #solar azimuth angle
    albedo = sky_models.albedo(color_coeff, rough_coeff, julian_day, time_instant, latitude) #water albedo
    I_dir, I_dif, I_gnd = sky_models.perez(time_data.day, time_data.month,
                                           time_instant, time_instant+0.99,
                                           np.abs(slope), azimuth, latitude, albedo,
                                           std_irradiance)
    slp_irradiance = np.sum([I_dir, I_dif, I_gnd]) #total irradiance using the perez anisotropic sky model

    if slp_irradiance < 0:
        slp_irradiance = 0 #accounting for possible negative numbers (irradiance shouldn't be smaller than 0)

    return std_irradiance, slp_irradiance

def power_output(eta, act_surface, sloped_irr, beta_temp, temp):
    '''
    :param eta: module efficiency
    :param act_surface: active surface of the pannel
    :param sloped_irr: irradiance for the inclined pannel
    :param beta_temp: temperature coefficient (Pmax)
    :param temp: current temperature
    :return: empirical power output of the FPV pannel
    '''
    return eta*act_surface*sloped_irr*(1-beta_temp*(temp-25)) #DC power output in [W]

def read_weather(conn, demo=False):
    '''
    :param conn: conn with the POSTGRESQL database
    :param demo: flag to monitor the demonstration set (False by default)
    :return: timescale data from the weather database
    '''
    cursor = conn.cursor()
    query = '''SELECT * FROM weather'''
    cursor.execute(query)
    timescale_data = cursor.fetchall()
    timescale_data = np.array(timescale_data)
    if demo:
        timescale_data = timescale_data[3:,:]
    return timescale_data

def fault_sched():
    short_circuit = {
        '25_11_2023': [np.arange(12, 16, 1), 2, 4],
        '28_11_2023': [np.arange(12, 16, 1), 0, 7],
        '30_11_2023': [np.arange(12, 16, 1), 1, 3],
        '25_12_2023': [np.arange(13, 16, 1), 0, 2],
        '27_12_2023': [np.arange(13, 16, 1), 2, 1],
        '29_12_2023': [np.arange(13, 16, 1), 1, 5]
    }
    open_circuit = {
        '1_12_2023': [np.arange(8, 13, 1), 0],
        '3_12_2023': [np.arange(8, 13, 1), 1],
        '7_12_2023': [np.arange(8, 13, 1), 2],
        '12_12_2023': [np.arange(9, 15, 1), 0],
        '13_12_2023': [np.arange(9, 15, 1), 1],
        '14_12_2023': [np.arange(9, 15, 1), 2]
    }
    degradation = {
        '23_11_2023': [np.arange(11, 12, 1), 2],
        '24_11_2023': [np.arange(11, 12, 1), 0],
        '26_11_2023': [np.arange(11, 12, 1), 1],
        '5_12_2023': [np.arange(10, 14, 1), 4],
        '8_12_2023': [np.arange(10, 14, 1), 7],
        '9_12_2023': [np.arange(10, 14, 1), 3]
    }
    shading = {
        '18_12_2023': [np.arange(8, 16, 1), 0, 0],
        '20_12_2023': [np.arange(8, 16, 1), 1, 4],
        '23_12_2023': [np.arange(8, 16, 1), 2, 3]
    }
    return short_circuit, open_circuit, degradation, shading

def run_simu(time, temp, wind_speed, cloud_cover, latitude, altitude, Ns, Np, broker_configs, conn):
    '''
    :param time: complete data containing time 
    :param temp: complete data containing temperature
    :param wind_speed: complete data containing wind speed
    :param cloud_cover: cloud cover ratio of the location
    :param latitude: latitude of the location
    :param altitude: altitude of the location
    :param Ns: number of PV cells in series in the string
    :param Np: number of PV strings
    :param broker_configs: dictionary containing mqtt broker configuration (host, port, keepalive and general topic)
    :param conn: connection with the POSTGRESQL database
    :return: registers the irradiances and inclinations on the database
    '''
    # checking for last database update
    abs_path = os.path.abspath('..')
    flag_path = './pre_proc_data/last_update.txt'
    time_path = './pre_proc_data/elapsed_time.txt'
    with open(flag_path, 'r') as read_flag:
        last_update = read_flag.readlines()
    if last_update == [str(time[-1])]:
        #update the last_update.txt file
        #with open(flag_path, 'w') as update:
        #    update.write('')
        #    update.write('2023-12-11 22:00:00+00:00')
        #update the elapsed_time.txt file
        #with open(time_path, 'w') as elapsed:
        #    elapsed.write('0')
        print('[offtwAIn_synthesizer] Database already updated!')
        return 0
    else:
        empty = False #flag to monitor file

        if os.stat(flag_path).st_size == 0: #check if .txt is empty
            last_update = [str(time[0])] #account for an empty .txt file (last_update = time[0])ex
            empty = True #update flag

        resolution = 1  # resolution of the data in hours
        time_search = datetime.fromisoformat(last_update[0])
        time_search = time_search.replace(tzinfo=timezone.utc)
        if not empty:
            idx_time = np.where(time == time_search+timedelta(hours=resolution))[0][0] #find the index of the last update from the database
            print(f'[offtwAIn_synthesizer] Running simulator from {time_search+timedelta(hours=resolution)}...')
        else:
            idx_time = np.where(time == time_search)[0][0]  # find the index of the last update from the database
            print(f'[offtwAIn_synthesizer] Running simulator from {time_search}...')

        #filter only non-computed data since the update
        time = time[idx_time+1:]
        temp = temp[idx_time+1:]
        wind_speed = wind_speed[idx_time+1:]

        #albedo computation info
        colour_coeff = 0.23 #waves, 1 inch or more with occasional whitecaps
        rough_coeff = 1.25 #waves, 1 inch or more with occasional whitecaps

        #FPV performance info
        eta = 0.1697  # module efficiency
        I_sc = 9.45  # short-circuit current
        V_oc = 45.6  # open-circuit current
        I_mp = 8.88  # optimum operating current
        V_mp = 37.2  # optimum operating current
        mu_Isc = (0.053 / 100)  # temperature Coefficient I_sc
        mu_Voc = (-0.31 / 100)  # temperature Coefficient V_oc

        # Reference SDM parameters
        ref_param = simulation_models.deSoto_ref_param(I_sc, V_oc, I_mp, V_mp, Ns)

        #Running the framework
        if os.stat(time_path).st_size == 0: #check if .txt is empty
            elapsed_time = [0] #account for an empty .txt file (elapsed_time = 0)
        else:
            with open(time_path, 'r') as read_time:
                elapsed_time = read_time.readlines()
        elapsed_time = float(elapsed_time[0]) #elapsed time of operation since the last update
        synthesis_data = [] #list that contains all the entries to the synthesis database
        model_res_data = [] #list that contains all the entries to the model_data database
        wave_props = load_wave_properties() #load wave properties data interpolated from Golroodbari and van Sark., 2020
        sc_sched, oc_sched, degrad_sched, shade_sched = fault_sched()  # dictionary containing the faults stamps
        for i in range(len(time)):
            #Environment computations
            curr_slope = 0.5*ocean_wave(wind_speed[i], wave_props) #irradiance for the inclined pannel
            curr_std_irr, curr_slp_irr = FPV_irradiance(time[i], latitude, altitude,
                                                       colour_coeff, rough_coeff, curr_slope, cloud_cover[i])
            # output computations
            p_out = np.zeros((Np, 1))  # [Np,1] array containing the sum of the power output of every cell at the sample
            corrosion_degradation = simulation_models.pan_corrosion(elapsed_time)  #corrosion_degradation(t)
            curr_query = {
                'time': time[i],
                'inclination': curr_slope,
                'irradiance': curr_slp_irr
            }
            res_query = {
                'time':  time[i]
            }
            for p in range(Np):
                for s in range(Ns):
                    # Managing flaws
                    if sc_sched.get(f'{time[i].day}_{time[i].month}_{time[i].year}') is not None:
                        curr_dict = sc_sched[f'{time[i].day}_{time[i].month}_{time[i].year}']
                        if p == curr_dict[1] and s == curr_dict[2] and time[i].hour in curr_dict[0]:
                            short_circuit = True
                        else:
                            short_circuit = False
                    else:
                        short_circuit = False

                    if oc_sched.get(f'{time[i].day}_{time[i].month}_{time[i].year}') is not None:
                        curr_dict = oc_sched[f'{time[i].day}_{time[i].month}_{time[i].year}']
                        if p == curr_dict[1] and time[i].hour in curr_dict[0]:
                            open_circuit = True
                        else:
                            open_circuit = False
                    else:
                        open_circuit = False

                    if degrad_sched.get(f'{time[i].day}_{time[i].month}_{time[i].year}') is not None:
                        curr_dict = degrad_sched[f'{time[i].day}_{time[i].month}_{time[i].year}']
                        if p == curr_dict[1] and time[i].hour in curr_dict[0]:
                            R_degrad = 6
                        else:
                            R_degrad = 0
                    else:
                        R_degrad = 0

                    if shade_sched.get(f'{time[i].day}_{time[i].month}_{time[i].year}') is not None:
                        curr_dict = shade_sched[f'{time[i].day}_{time[i].month}_{time[i].year}']
                        if p == curr_dict[1] and time[i].hour in curr_dict[0]:
                            shade = True
                            if s == curr_dict[2]:
                                shade_panel = True
                            else:
                                shade_panel = False
                        else:
                            shade = False
                            shade_panel = False
                    else:
                        shade = False
                        shade_panel = False

                    #calculate the outputs of the simulator (power, current and voltage of the panel)
                    power_output_cell, curr_v_out, curr_i_out, curr_fault_label = \
                        simulation_models.cell_output(time[i], ref_param, temp[i], curr_slp_irr,
                                                      mu_Isc, mu_Voc, eta, I_mp, V_mp,
                                                      R_degrad=R_degrad, open_circ_flag=open_circuit,
                                                      short_circ_flag=short_circuit,
                                                      shade_flag=shade, shade_panel_flag=shade_panel)
                    p_out[p] += power_output_cell

                    #setting up the current query dictionary keys
                    i_key = f'i_OT{p}{s}_dc'  #current output key
                    v_key = f'v_OT{p}{s}_dc'  #voltage output key
                    f_key = f'fault_OT{p}{s}' #fault label output key

                    #current query dictionary
                    curr_query[i_key] = float(curr_i_out)
                    curr_query[v_key] = float(curr_v_out)
                    curr_query[f_key] = int(curr_fault_label)

                    #MQTT message
                    curr_msg = message_editor(curr_query, p, s, corrosion_degradation) #edit the mqtt message
                    mqtt_publisher(broker_configs, curr_msg, p) #send the message
                    
                p_out[p] = p_out[p] * (1 - corrosion_degradation)
                curr_p_hat, curr_res, curr_res_mean, curr_var_res, curr_status = \
                    simulation_models.check_string_status(conn, p, curr_slp_irr, p_out[p], 0.8, time[i]) #check the status of the string
                curr_label_hat = simulation_models.fault_label(curr_query, p_out[p], curr_status, p, time[i]) #predicted fault label

                #String fault status MQTT message
                curr_fault_msg = fault_message_editor(time[i], curr_status, curr_label_hat, p) #edit the fault mqtt message
                mqtt_fault_publisher(broker_configs, curr_fault_msg, p) #send the fault status message

                #Update Tables
                #synthetic
                p_key = f'p_OT{p}_dc'  # power output key
                status_key = f'status_OT{p}' # string status flag
                curr_query[p_key] = float(p_out[p][0])
                curr_query[status_key] = curr_status
                #model_data
                mean_res_key = f'mean_residue_OT{p}' #recursive mean of the residues
                var_res_key = f'var_residue_OT{p}' #recursive variance of the residues
                res_key = f'residue_OT{p}' #residue
                p_hat_key = f'p_hat_OT{p}' #estimated power output of the string
                res_query[mean_res_key] = curr_res_mean
                res_query[var_res_key] = curr_var_res
                res_query[res_key] = curr_res[0]
                res_query[p_hat_key] = curr_p_hat

            insert_synthetic_data(conn, 'synthetic', curr_query)
            insert_model_data(conn, 'model_data', res_query)
            elapsed_time += resolution  # increment the elapsed time counter with the resolution

        #update the last_update.txt file
        with open(flag_path, 'w') as update:
            update.write(str(time[-1]))
        #update the elapsed_time.txt file
        with open(time_path, 'w') as elapsed:
            elapsed.write(str(elapsed_time))

        return synthesis_data, model_res_data

def message_editor(query, string, panel, corrosion):
    '''
    :param query: dictionary with the outputs of the simulator at the current [pannel] of the [string]
    :param string: number of the current string being computed [0, Np-1]
    :param panel: number of the current panel being computed [0, Ns-1]
    :param corrosion: corrosion_degradation(t)
    :param bool_status: status of the panel (1 -> fault and 0 -> normal op)
    :return: dictionary with some IoT infos and the output of the simulator for the current [panel]
    '''
    mqtt_message = {
        'thingId': f'offtwAIn_panel:F1-S{string}-P{panel}',
        'series': f'F1-S{string}',
        'panel': f'F1-S{string}-P{panel}',
        'time': query['time'],
        'inclination': query['inclination'],
        'irradiance': query['irradiance'],
        'i_dc': query[f'i_OT{string}{panel}_dc'],
        'v_dc': query[f'v_OT{string}{panel}_dc'],
        'p_dc': float((1-corrosion)*query[f'i_OT{string}{panel}_dc']*query[f'v_OT{string}{panel}_dc']),
    }

    return mqtt_message

def fault_message_editor(timestamp, status, label, string):
    '''
    :param time: current timestamp of the sample
    :param status: fault status flag (True -> fault; False -> normal op)
    :param label: label of the fault status
    NO -> normal operation
    DG -> degradation
    OC -> open-circuit
    SC -> short-circuit
    SD -> shading
    :param string: number of the current string being computed [0, Np-1]
    :return: dictionary with some IoT infos and the output of the simulator for the current [panel]
    '''
    mqtt_fault_message = {
        'thingId': f'offtwAIn_string:F1-S{string}',
        'series': f'F1-S{string}',
        'time': timestamp,
        'fault_status': status,
        'fault_label': label
    }

    return mqtt_fault_message

def on_message(client, userdata, msg):
    payload = msg.payload.decode('utf-8')
    print(payload)

def json_serial(obj): #function to handle datetime data entries (convert to json serializable)
    '''
    :param obj: json dictionary with the output data of the simulator
    :return: deals with timestamp data format to be serialized
    '''
    if isinstance(obj, datetime):
        return obj.isoformat()

def mqtt_publisher(broker_configs, simulator_data, string):
    print("ENTROU")
    '''
    :param broker_configs: dictionary containing mqtt broker configuration (host, port, keepalive and general topic)
    :param simulator_data: dictionary with some IoT infos and the output of the simulator
    :param string: number of the string [0, Np-1]
    '''
    client = mqtt.Client()
    client.username_pw_set(broker_configs['mqtt_username'], broker_configs['mqtt_password'])
    client.on_message = on_message  # print the message content each message

    if client.connect(host=broker_configs['host'], port=broker_configs['port'], keepalive=broker_configs['keepalive']) != 0:
        print('[offtwAIn_synthesizer] Could not connect to the MQTT broker!') #handles connection issues
    
    print(f"{simulator_data['thingId'].replace(':', '/')}")
    payload = json.dumps(simulator_data, default=json_serial)  # simulaton data to be sent via mqtt
    print(payload)
    client.publish(f"{simulator_data['thingId'].replace(':', '/')}", payload, qos=1)  #send the msg at least one time
    time.sleep(0.2)
    client.disconnect()  #disconnect from the broker

def mqtt_fault_publisher(broker_configs, simulator_data, string):
    '''
    :param broker_configs: dictionary containing mqtt broker configuration (host, port, keepalive and general topic)
    :param status_data: dictionary with some IoT infos and the output of the fault identification
    :param string: number of the string [0, Np-1]
    '''
    client = mqtt.Client()  #connect to the mosquitto broker
    client.on_message = on_message  # print the message content each message
    client.subscribe(f"{broker_configs['status_topic']}{string}")  #topic of the simulator results (one per string)

    if client.connect(host=broker_configs['host'], port=broker_configs['port'], keepalive=broker_configs['keepalive']) != 0:
        print('[offtwAIn_synthesizer] Could not connect to the MQTT broker!') #handles connection issues
    payload = json.dumps(simulator_data, default=json_serial)  # simulaton data to be sent via mqtt
    client.publish(f"{simulator_data['thingId'].replace(':', '/')}", payload, qos=1)  #send the msg at least one time
    client.disconnect()  #disconnect from the broker

def insert_synthetic_data(conn, table, data):
    '''
    :param conn: connection with the POSTGRESQL database
    :param table: name of the table in the PSQL database
    :param synthetic_data: output of the simulator for the current sample
    '''
    with conn.cursor() as cursor:
        #for data in synthetic_data:
        cursor.execute(
                sql.SQL("INSERT INTO {} ("
                        "time, inclination, irradiance, "
                        "i_OT00_dc, v_OT00_dc, fault_OT00,"
                        "i_OT01_dc, v_OT01_dc, fault_OT01,"
                        "i_OT02_dc, v_OT02_dc, fault_OT02,"
                        "i_OT03_dc, v_OT03_dc, fault_OT03, "
                        "i_OT04_dc, v_OT04_dc, fault_OT04, "
                        "i_OT05_dc, v_OT05_dc, fault_OT05, "
                        "i_OT06_dc, v_OT06_dc, fault_OT06, "
                        "i_OT07_dc, v_OT07_dc, fault_OT07, p_OT0_dc, status_OT0, "
                        "i_OT10_dc, v_OT10_dc, fault_OT10, "
                        "i_OT11_dc, v_OT11_dc, fault_OT11, "
                        "i_OT12_dc, v_OT12_dc, fault_OT12, "
                        "i_OT13_dc, v_OT13_dc, fault_OT13, "
                        "i_OT14_dc, v_OT14_dc, fault_OT14, "
                        "i_OT15_dc, v_OT15_dc, fault_OT15, "
                        "i_OT16_dc, v_OT16_dc, fault_OT16, "
                        "i_OT17_dc, v_OT17_dc, fault_OT17, p_OT1_dc, status_OT1,"
                        "i_OT20_dc, v_OT20_dc, fault_OT20, "
                        "i_OT21_dc, v_OT21_dc, fault_OT21, "
                        "i_OT22_dc, v_OT22_dc, fault_OT22, "
                        "i_OT23_dc, v_OT23_dc, fault_OT23, "
                        "i_OT24_dc, v_OT24_dc, fault_OT24, "
                        "i_OT25_dc, v_OT25_dc, fault_OT25, "
                        "i_OT26_dc, v_OT26_dc, fault_OT26, "
                        "i_OT27_dc, v_OT27_dc, fault_OT27, p_OT2_dc, status_OT2)"
                        "VALUES ("
                        "%s, %s, %s, "
                        "%s, %s, %s,"
                        "%s, %s, %s,"
                        "%s, %s, %s,"
                        "%s, %s, %s, "
                        "%s, %s, %s, "
                        "%s, %s, %s, "
                        "%s, %s, %s, "
                        "%s, %s, %s, %s, "
                        "%s, %s, %s, "
                        "%s, %s, %s, "
                        "%s, %s, %s, "
                        "%s, %s, %s, "
                        "%s, %s, %s, "
                        "%s, %s, %s, "
                        "%s, %s, %s, "
                        "%s, %s, %s, %s, "
                        "%s, %s, %s, "
                        "%s, %s, %s, "
                        "%s, %s, %s, "
                        "%s, %s, %s, "
                        "%s, %s, %s, "
                        "%s, %s, %s, "
                        "%s, %s, %s, "
                        "%s, %s, %s, %s,"
                        "%s, %s, %s) ON CONFLICT (time) DO NOTHING;")
                        .format(sql.Identifier(table)),
                        (data['time'], data['inclination'], data['irradiance'],
                         data['i_OT00_dc'], data['v_OT00_dc'], data['fault_OT00'],
                         data['i_OT01_dc'], data['v_OT01_dc'], data['fault_OT01'],
                         data['i_OT02_dc'], data['v_OT02_dc'], data['fault_OT02'],
                         data['i_OT03_dc'], data['v_OT03_dc'], data['fault_OT03'],
                         data['i_OT04_dc'], data['v_OT04_dc'], data['fault_OT04'],
                         data['i_OT05_dc'], data['v_OT05_dc'], data['fault_OT05'],
                         data['i_OT06_dc'], data['v_OT06_dc'], data['fault_OT06'],
                         data['i_OT07_dc'], data['v_OT07_dc'], data['fault_OT07'], data['p_OT0_dc'], data['status_OT0'],
                         data['i_OT10_dc'], data['v_OT10_dc'], data['fault_OT10'],
                         data['i_OT11_dc'], data['v_OT11_dc'], data['fault_OT11'],
                         data['i_OT12_dc'], data['v_OT12_dc'], data['fault_OT12'],
                         data['i_OT13_dc'], data['v_OT13_dc'], data['fault_OT13'],
                         data['i_OT14_dc'], data['v_OT14_dc'], data['fault_OT14'],
                         data['i_OT15_dc'], data['v_OT15_dc'], data['fault_OT15'],
                         data['i_OT16_dc'], data['v_OT16_dc'], data['fault_OT16'],
                         data['i_OT17_dc'], data['v_OT17_dc'], data['fault_OT17'], data['p_OT1_dc'], data['status_OT1'],
                         data['i_OT20_dc'], data['v_OT20_dc'], data['fault_OT20'],
                         data['i_OT21_dc'], data['v_OT21_dc'], data['fault_OT21'],
                         data['i_OT22_dc'], data['v_OT22_dc'], data['fault_OT22'],
                         data['i_OT23_dc'], data['v_OT23_dc'], data['fault_OT23'],
                         data['i_OT24_dc'], data['v_OT24_dc'], data['fault_OT24'],
                         data['i_OT25_dc'], data['v_OT25_dc'], data['fault_OT25'],
                         data['i_OT26_dc'], data['v_OT26_dc'], data['fault_OT26'],
                         data['i_OT27_dc'], data['v_OT27_dc'], data['fault_OT27'], data['p_OT2_dc'], data['status_OT2'])
            )
        conn.commit()
        #print('[offtwAIn_synthesizer] Synthetic data simulation complete!')

def insert_model_data(conn, table, data):
    with conn.cursor() as cursor:
        #for data in model_data:
        cursor.execute(
            sql.SQL("INSERT INTO {} ("
                    "time, mean_residue_OT0, var_residue_OT0, residue_OT0, p_hat_OT0, "
                    "mean_residue_OT1, var_residue_OT1, residue_OT1, p_hat_OT1,"
                    "mean_residue_OT2, var_residue_OT2, residue_OT2, p_hat_OT2)"
                    "VALUES ("
                    "%s, %s, %s,"
                    "%s, %s, %s,"
                    "%s, %s, %s, %s,"
                    "%s, %s, %s) ON CONFLICT (time) DO NOTHING;")
                .format(sql.Identifier(table)),
            (data['time'], data['mean_residue_OT0'], data['var_residue_OT0'], data['residue_OT0'], data['p_hat_OT0'],
             data['mean_residue_OT1'], data['var_residue_OT1'], data['residue_OT1'], data['p_hat_OT1'],
             data['mean_residue_OT2'], data['var_residue_OT2'], data['residue_OT2'], data['p_hat_OT2'])
        )
        conn.commit()
    #print('[offtwAIn_synthesizer] Model data simulation complete!')

def read_synthetic(conn, table, save=False):
    '''
    :param conn: connection with the POSTGRESQL database
    :param save: save the database content in a .npy file
    :return: timescale data from the weather database
    '''
    print('[offtwAIn_synthesizer] Reading from the synthetic database...')
    cursor = conn.cursor()
    query = f'''SELECT * FROM {table}'''
    cursor.execute(query)
    synthetic_data = cursor.fetchall()
    synthetic_data = np.array(synthetic_data)

    # saving the content
    if save:
        absolute_path = os.path.abspath('..')
        np.save(os.path.join(f'./pre_proc_data/{table}_data'), synthetic_data)

    print('[offtwAIn_synthesizer] Synthetic database read!')

    return synthetic_data
