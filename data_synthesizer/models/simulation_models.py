import time

import numpy as np
import pandas as pd
import matplotlib
matplotlib.use('TkAgg') #pycharm bug fix
import matplotlib.pyplot as plt
import scipy.io
import os
from scipy.optimize import fsolve
from models import sky_models

def read_data():
    #reading the pv_faultset data
    absolute_path = os.path.abspath('..')
    data_amb_path = os.path.join(absolute_path, 'data/dataset_amb.mat')
    data_amb = scipy.io.loadmat(data_amb_path)
    data_elec_path = os.path.join(absolute_path, 'data/dataset_elec.mat')
    data_elec = scipy.io.loadmat(data_elec_path)

    #creating the variables of interest
    v_dc1 = data_elec['vdc1'].T #DC voltage string 1 [V]
    i_dc1 = data_elec['idc1'].T #DC current string 1 [A]
    v_dc2 = data_elec['vdc2'].T #DC voltage string 2 [V]
    i_dc2 = data_elec['idc2'].T #DC current string 2 [A]
    g = data_amb['irr'].T #irradiance [W/m^2]
    temp = data_amb['pvt'].T #temperature [°C]
    return v_dc1, i_dc1, v_dc2, i_dc2, g, temp

def exp_irr_model(phi, N, time):
    '''
    :param phi: latitude of the observer [degrees]
    :param N: julian date [int]
    :param time: time in decimals [float]
    :return: solar irradiance at the latitude [W/m^2]
    '''
    #air mass ratio
    alpha = sky_models.altitude(phi, N, time)
    air_ratio = 1/np.sin(np.deg2rad(alpha))

    #apparent extraterrestrial flux
    app_flux = 1160 + (75*np.sin(2*np.pi/365*(N-275)))

    #optical depth
    opp_dep = 0.174+(0.035*np.sin(2*np.pi/365*(N-100)))

    #compensating for sunrise/sundown
    delta = sky_models.declination(N)
    sun_presence = -np.tan(np.deg2rad(phi)) * np.tan(np.deg2rad(delta))
    omega_s = np.rad2deg(np.arccos(sun_presence))
    time_r_d = 12 - omega_s / 15
    if time < time_r_d or time > 12 + (omega_s / 15):
        daytime_flag = 0
    else:
        daytime_flag = (alpha > 0) #1 if daytime, 0 if not

    #radiation computation
    return app_flux * np.exp(-1 * opp_dep * air_ratio) * daytime_flag

def vanDam_irr_model(phi, beta, tau, N, time, h, cloud_cover_ratio):
    '''
    :param phi: latitude of the observer [degrees]
    :param beta: slope of the surface [degrees]
    :param tau: transmissivity [float]
    :param N: julian date [int]
    :param time: time in decimals [float]
    :param h: altitude of the surface [float]
    :param cloud_cover_ratio: cloud cover ratio of the location [%]
    :return:
    '''
    #angles
    alpha = sky_models.altitude(phi, N, time) #solar altitude
    alpha = np.deg2rad(alpha) #degrees to radians
    theta = sky_models.incidence(beta, phi, N, time) #incidence of the solar rays
    theta = np.deg2rad(theta) #degrees to radians

    #Solar radiation from outer surface of the atmosphere
    S_out_part1 = 1 + (0.033 * np.cos(np.deg2rad((360*N)/365)))
    S_out = 1367*S_out_part1

    #Solar radiation that reaches the Earth's surface
    M_o_part1 = 1229+((614*np.sin(alpha))**2)
    M_o_part2 = 614*np.sin(alpha)
    M_o = np.sqrt(M_o_part1) - M_o_part2 #relative path length of the optical air mass (sea level)
    P_h_o = ((288 - 0.0065*h)/288)**5.256 #atmospheric pressure correction
    M_h = M_o*P_h_o #relative path length of the optical air mass
    S_nor = S_out * (tau**M_h) #radiation flux through the normal plane
    S_dir = S_nor * np.cos(theta)

    #Direct light scattering
    S_dif = S_out * (0.271 - 0.294*(tau**M_h)) * np.sin(alpha)

    # Cloud coverage effect
    cloud_cover_ratio /= 100  # percentage to ratio
    if cloud_cover_ratio >= 0.95:
        # overcast sky: cloud coverage of at least 95%
        S_dir = 0
    else:
        S_dir *= (1 - cloud_cover_ratio)

    # Total irradiation
    S_total = S_dir + S_dif  # global component of irradiation

    if S_total < 0:
        return 0
    else:
        return S_total

def pan_corrosion(t):
    '''
    :param t: elapsed time since the beggining of plant operation [hours]
    :return: corrosion degradation model proposed by Pan et al., 2011 [%]
    '''
    #parameters of the model (Charki et al., 2013)
    #PV module: polycrystalline silicon
    a_corr = 3.0868
    b_corr = 5.762e-12

    return 1 - np.exp(-b_corr * (t**a_corr))

def RLS(v_dc, i_dc, g, plot=True):
    '''
    :param vdc1: DC Voltage from string 1 of the dataset [float]
    :param idc1: DC Current from string 1 of the dataset [float]
    :param vdc2: DC Voltage from string 2 of the dataset [float]
    :param idc2: DC Current from string 2 of the dataset [float]
    :param g: Irradiance on the pannels [float]
    :return: ARX model predictions
    '''
    #creating dinamic arrays
    p_dc_hat = list()  # predicted value

    #Control variables for the RLS algorithm
    p_dc_meas = v_dc*i_dc #measured output power
    reg = 4 #number of regressors
    theta = np.zeros((reg,1)) #parameters vector
    cov = 1000*np.eye(len(theta)) #covariance matrix
    p_dc_hat.append(p_dc_meas[0])
    p_dc_hat.append(p_dc_meas[1])

    #Recursive least squares algorithm
    for i in range(2,np.shape(p_dc_meas)[0]):
        psi = np.array([[p_dc_hat[i - 1][0]],
                        [p_dc_hat[i - 2][0]],
                        [g[i][0]],
                        [g[i-1][0]],
                        ])  # measurements vector
        K = cov @ psi * (1 + psi.T @ cov @ psi) ** (-1)  # gains
        n = p_dc_meas[i] - psi.T @ theta  # innovation
        theta += K * n  # parameters update
        cov -= K @ psi.T @ cov  # covariance matrix update
        p_dc_hat.append((psi.T @ theta)[0]) #prediction update

    #Fixing array complications
    p_dc_hat = [float(p_dc) for pred in p_dc_hat for p_dc in pred if isinstance(p_dc, (int, float))]

    if plot:
        plt.figure()
        leg = list()
        plt.plot(p_dc_meas)
        leg.append('Measured')
        plt.plot(p_dc_hat)
        leg.append('RLS')
        plt.legend(leg)
        plt.xlabel('Index')
        plt.ylabel('Output [W]')

    return p_dc_hat

def RLS_p(p_dc_meas, g):
    '''
    :param vdc1: DC Voltage from string 1 of the dataset [float]
    :param idc1: DC Current from string 1 of the dataset [float]
    :param vdc2: DC Voltage from string 2 of the dataset [float]
    :param idc2: DC Current from string 2 of the dataset [float]
    :param g: Irradiance on the pannels [float]
    :return: ARX model predictions
    '''
    #creating dinamic arrays
    p_dc_hat = list()  # predicted value

    #Control variables for the RLS algorithm
    reg = 4 #number of regressors
    theta = np.zeros((reg,1)) #parameters vector
    cov = 1000*np.eye(len(theta)) #covariance matrix
    p_dc_hat.append(p_dc_meas[0])
    p_dc_hat.append(p_dc_meas[1])

    #Recursive least squares algorithm
    for i in range(2,np.shape(p_dc_meas)[0]):
        psi = np.array([[p_dc_hat[i - 1][0]],
                        [p_dc_hat[i - 2][0]],
                        [g[i][0]],
                        [g[i-1][0]],
                        ])  # measurements vector
        K = cov @ psi * (1 + psi.T @ cov @ psi) ** (-1)  # gains
        n = p_dc_meas[i] - psi.T @ theta  # innovation
        theta += K * n  # parameters update
        cov -= K @ psi.T @ cov  # covariance matrix update
        p_dc_hat.append((psi.T @ theta)[0]) #prediction update

    #Fixing array complications
    p_dc_hat = [float(p_dc) for pred in p_dc_hat for p_dc in pred if isinstance(p_dc, (int, float))]
    return p_dc_hat

def deSoto_system(p, I_sc, V_oc, I_mp, V_mp):
    '''
    :param I_sc: Short-circuit current
    :param V_oc: Open-circuit voltage
    :param I_mp: Operating current at the max. power
    :param V_mp: Operating current at the max. power
    :param mu_Voc: temperature coeff. of open-circuit voltage
    :return: non-linear system of equations based on the kirchoff law of currents for the single diode model
    '''
    I_l, I_o, a, R_s, R_sh = p
    eq_sist = (
        I_sc - I_l + I_o*(np.exp(I_sc*R_s/a) - 1) + (I_sc/R_s)/R_sh,  # 'short-circuit condition (V = 0)
        I_l - I_o*(np.exp(V_oc/a) - 1) - V_oc/R_sh,  # 'open-circuit condition (I = 0)
        I_mp - I_l + I_o*(np.exp((V_mp + I_mp*R_s)/a) - 1) + ((V_mp + I_mp*R_s)/R_sh),  # 'max. power condition
        I_mp - V_mp*((I_o/a)*np.exp((V_mp + I_mp*R_s)/a) + 1/R_sh) /
        (1 + (I_o*R_s/a)*np.exp((V_mp + I_mp*R_s)/a) + R_s/R_sh),  # 'derivative of power with respect to voltage
        0.5*(I_mp - I_sc) - ((I_o/a) * np.exp(I_sc * R_s/a) + 1/R_sh) /
        (1 + (I_o*R_s/a)*np.exp(I_sc*R_s/a) + R_s/R_sh)  # 'slope of the I-V curve at the short-circuit
    )
    return eq_sist

def deSoto_ref_param(I_sc, V_oc, I_mp, V_mp, Ns):
    '''
    :param I_sc: Short-circuit current
    :param V_oc: Open-circuit voltage
    :param I_mp: Operating current at the max. power
    :param V_mp: Operating current at the max. power
    :param Ns: number of PV cells in series
    :return: The five reference parameters for the single diode model
    '''
    #Constants
    k = 1.381e-23 #boltzmann constant [J/K]
    q = 1.602e-19 #electron charge [C]
    T_ref = 273.15 + 25 #reference temperature of 25 degrees celsius

    #Define the initial guesses for the fsolve method
    R_sh_ref = 100 #Shunt resistor
    a_ref = 1.5*k*T_ref*Ns/q #ideality factor
    I_l_ref = I_sc #Photo-current
    I_o_ref = I_sc*np.exp(-V_oc/a_ref) #Reverse diode sat. current
    R_s_half = a_ref*np.log((I_l_ref-I_mp+I_o_ref)/I_o_ref)-V_mp
    R_s_ref = R_s_half/I_mp #series resistor
    initial_guess = np.array([I_l_ref, I_o_ref, a_ref, R_s_ref, R_sh_ref])

    #Solving the non-linear system for the parameters
    output = fsolve(deSoto_system, initial_guess, args=(I_sc, V_oc, I_mp, V_mp))
    return output

def deSoto_operation(p, I_l, I_o, a, R_s, R_sh):
    '''
    :param I_l: photo-current
    :param I_o: diode reverse saturation current
    :param a: ideality factor
    :param R_s: series resistance
    :param R_sh: shunt resistance
    :return: non-linear system of equations for the operation conditions
    '''
    I_op, V_op = p
    eq_sist = (
        I_op - I_l + I_o*(np.exp((V_op + I_op*R_s)/a) - 1) - ((V_op + I_op*R_s)/R_sh),  # 'general eq. at operation
        I_op - V_op*((I_o / a) * np.exp((V_op + I_op * R_s) / a) + 1 / R_sh) /
        (1 + (I_o * R_s / a) * np.exp((V_op + I_op * R_s) / a) + R_s / R_sh)  # 'derivative of power w/ resp. to volt_op
    )
    return eq_sist

def deSoto_shade_operation(p, I_l, I_o, a, R_s, R_sh, I_op):
    '''
    :param I_l: photo-current
    :param I_o: diode reverse saturation current
    :param a: ideality factor
    :param R_s: series resistance
    :param R_sh: shunt resistance
    :param I_op: Operation current at the given moment
    :return: Output voltage of the panel
    '''
    V_op = p
    eq_sist = (
        I_op - I_l + I_o * (np.exp((V_op + I_op * R_s) / a) - 1) - ((V_op + I_op * R_s) / R_sh)
    )
    return eq_sist

def deSoto_SDM(I_l_ref, I_o_ref, a_ref, R_s_ref, R_sh_ref, T, Irr, mu_Isc, I_mp, V_mp,
               R_degrad=None, open_circ=False, short_circ=False, shade=False, shade_panel=False):
    '''
    :param I_l_ref: reference photo-current
    :param I_o_ref: reference diode reverse saturation current
    :param a_ref: reference ideality factor
    :param R_s_ref: reference series resistance
    :param R_sh_ref: reference shunt resistance
    :param T: sample temperature
    :param Irr: irradiance in [W/m^2]
    :param mu_Isc: temperature coefficient of the short circuit current
    :param I_mp: Operation current at the maximum power
    :param V_mp: Operation voltage at the maximum power
    :param R_degrad: degradation series resistance to manage flaw synthesis in the system
    :param open_circ: open-circuit flag, if True sets I_op and V_op to 0
    :param short_circ: short-circuit flag, if True sets V_op to 0 and keeps the I_op value
    :param shade: shading flag to manage the effect of partial shading in the system
    :param shade_panel:flag that represents if the panel of the string is the one being shaded
    :return: The five parameters for the single diode model at the current operation (I_op and V_op)
    '''
    #Constants
    k = 1.381e-23  # boltzmann constant [J/K]
    q = 1.602e-19  # electron charge [C]
    T += 273.15 #celsius to Kelvin
    T_ref = 273.15 + 25  # reference temperature of 25 degrees celsius
    Irr_ref = 1000  # reference irradiance in [W/m^2]
    Eg_ref = 1.12 #band gap for polycristalline structures
    C = -0.0002 #derivative of the band gap for polycristalline structures (C constant)
    beta = 0.217 #relative temperature coefficient (zhu et al., 2018)

    #Calculate the model's parameters at the operation temperature
    a = a_ref*T/T_ref #ideality factor
    Eg = Eg_ref*(1 - C*(T - T_ref)) #material band-gap
    I_o = I_o_ref*((T/T_ref)**3) * np.exp((q/k)*((Eg_ref/T_ref) - (Eg/T))) #diode reverse sat. current
    I_l = (Irr/Irr_ref)*(I_l_ref + mu_Isc*(T - T_ref)) #photo-current
    R_sh = R_sh_ref/(Irr/Irr_ref) #shunt resistor
    R_s = R_s_ref #series resistor
    if R_sh == np.inf:
        R_sh = R_sh_ref
    elif R_sh == 0:
        R_sh = R_sh_ref

    #Calculate the operation condition
    initial_guess = np.array([I_mp, V_mp])
    if open_circ:
        operation_out = [0,0] #open-circuit
    else:
        if R_degrad is not None:
            if shade:
                shaded_proportion = 0.6 #(60% shading)
                a *= (1 - shaded_proportion)  #shaded ideality factor
                I_l = (Irr / Irr_ref) * (
                        (1 - shaded_proportion) * I_l_ref + mu_Isc * (T - T_ref))  # shaded photo-current
                R_s = (1 - beta * np.log(
                    1 - shaded_proportion)) * R_s_ref  # shaded area relationship w/ series resistor
                R_sh *= (1 - shaded_proportion) #shaded shunt resistor
                operation_out = fsolve(deSoto_operation, initial_guess, args=(I_l, I_o, a, R_s, R_sh))
                if not shade_panel:
                    #compute the voltage output for the pannel given the shading current
                    a = a_ref * T / T_ref  # ideality factor
                    Eg = Eg_ref * (1 - C * (T - T_ref))  # material band-gap
                    I_o = I_o_ref * ((T / T_ref) ** 3) * np.exp(
                        (q / k) * ((Eg_ref / T_ref) - (Eg / T)))  # diode reverse sat. current
                    I_l = (Irr / Irr_ref) * (I_l_ref + mu_Isc * (T - T_ref))  # photo-current
                    R_sh = R_sh_ref / (Irr / Irr_ref)  # shunt resistor
                    R_s = R_s_ref  # series resistor
                    initial_guess = np.array([V_mp])
                    operation_shade = fsolve(deSoto_shade_operation, initial_guess,
                                           args=(I_l, I_o, a, R_s, R_sh, operation_out[0]))

                    if operation_shade[0] < 0:
                        operation_shade[0] = 0

                    operation_out[1] = operation_shade[0] #update output with new voltage given shading current
            else:
                operation_out = fsolve(deSoto_operation, initial_guess, args=(I_l, I_o, a, R_s + R_degrad, R_sh))
        else:
            operation_out = fsolve(deSoto_operation, initial_guess, args=(I_l, I_o, a, R_s, R_sh))

    if operation_out[0] < 0:
        operation_out[0] = 0
    elif operation_out[1] < 0:
        operation_out[1] = 0

    cols = ['I_l', 'I_o', 'a', 'R_s', 'R_sh', 'I_op', 'V_op']
    if short_circ:
        output = [I_l, I_o, a, R_s, R_sh, operation_out[0], 0]
    else:
        output = [I_l, I_o, a, R_s, R_sh, operation_out[0], operation_out[1]]

    output = pd.DataFrame([output])
    output.columns = cols

    return output

def eta_temp(V_mp, eta, mu_Voc, temp):
    '''
    :param V_mp: optimum operating voltage
    :param eta: module efficiency
    :param mu_Voc: temperature coefficient V_oc
    :param temp: temperature of the current sample
    :return: temperature dependence on the efficiency
    '''
    mu_temp = eta * mu_Voc / V_mp  # temperature coefficient at the operation point
    return eta - mu_temp * (temp - 25)  # efficiency at the operation point

def cell_output(time_data, ref_param, T, Irr, mu_Isc, mu_Voc, eta, I_mp, V_mp,
                R_degrad=None, open_circ_flag=False, short_circ_flag=False, shade_flag=False, shade_panel_flag=False):
    '''
    :param time: timestamp of the sample
    :param ref_param: The five reference parameters for the single diode model
    :param T: sample temperature
    :param Irr: irradiance in [W/m^2]
    :param mu_Isc: temperature coefficient of the short circuit current
    :param mu_Voc: temperature coefficient V_oc
    :param eta: module efficiency
    :param I_mp: Operation current at the maximum power
    :param V_mp: Operation voltage at the maximum power
    :param R_degrad: degradation series resistance to manage flaw synthesis in the system (None/0 = no degradation)
    :param open_circ_flag: open-circuit flag, if True sets I_op and V_op to 0
    :param short_circ_flag: short-circuit flag, if True sets V_op to 0 and keeps the I_op value
    :param shade_flag: shading flag to manage the effect of partial shading in the system
    :param shade_panel_flag: flag that represents if the panel of the string is the one being shaded
    :return: DC power, voltage, current output and fault label for the cell at the sample instant
    '''
    #output computations
    SDM_output = deSoto_SDM(ref_param[0], ref_param[1], ref_param[2], ref_param[3], ref_param[4],
                            T, Irr, mu_Isc, I_mp, V_mp,
                            R_degrad=R_degrad, open_circ=open_circ_flag, short_circ=short_circ_flag,
                            shade=shade_flag, shade_panel=shade_panel_flag)

    #managing flaws
    # [0 -> normal operation
    #  1 -> use degradation
    #  2 -> open-circuit
    #  3 -> short-circuit
    #  4 -> shading]
    if R_degrad is not None and R_degrad != 0:
        fault_label = 1
    elif open_circ_flag:
        fault_label = 2
    elif short_circ_flag:
        fault_label = 3
    elif shade_flag:
        fault_label = 4
    else:
        fault_label = 0

    #Managing weird convergence errors
    if time_data.hour <= 7 or time_data.hour > 18:
        if SDM_output['I_op'].values[0] > 0.5 or SDM_output['V_op'].values[0] > 0.5:
            SDM_output['I_op'].values[0] = 0
            SDM_output['V_op'].values[0] = 0
    elif SDM_output['I_op'].values[0] > 10:
        SDM_output['I_op'].values[0] = (I_mp + ((T - 25) * T * mu_Isc)) * .41
        SDM_output['V_op'].values[0] = V_mp + ((T - 25) * T * mu_Voc)


    eff_temp = eta_temp(V_mp, eta, mu_Voc, T)  #efficiency at the operation point
    power_out = (1 - eff_temp) * SDM_output['I_op'].values[0] * SDM_output['V_op'].values[0]
    voltage_out = SDM_output['V_op'].values[0]
    current_out = SDM_output['I_op'].values[0]

    return power_out, voltage_out, current_out, fault_label

def recursive_LS(curr_irr, past_irr, p_hat, p_meas, string):
    '''
    :param curr_irr: current irradiance for the inclined pannel
    :param past_irr: past values of irradiance for the ARX model
    :param p_hat: past values of predicted power output
    :param p_meas: measured (synthetic) values of power output
    :param string: number of the current string being computed [0, Np-1]
    :return: the predicted power output for the current sample
    '''
    abs_path = os.path.abspath('..')
    theta = np.load(os.path.join(f'./pre_proc_data/past_thetas/last_theta_OT{string}.npy'))
    cov = np.load(os.path.join(f'./pre_proc_data/past_covariances/last_cov_OT{string}.npy'))
    psi = np.array([[p_hat[-1]],
                    [p_hat[-2]],
                    [curr_irr],
                    [past_irr[-1]],
                    [past_irr[-2]]])  # measurements vector
    K = cov @ psi * (1 + psi.T @ cov @ psi) ** (-1)  # gains
    n = p_meas - psi.T @ theta  # innovation
    theta += K * n  # parameters update
    cov -= K @ psi.T @ cov  # covariance matrix update

    # Update stored covariance and theta
    np.save(os.path.join(f'./pre_proc_data/past_covariances/last_cov_OT{string}'),cov)
    np.save(os.path.join(f'./pre_proc_data/past_thetas/last_theta_OT{string}'), theta)

    if (psi.T @ theta)[0][0] < 0:
        return 0  #prediction update
    elif (psi.T @ theta)[0][0] >= 0:
        return ((psi.T @ theta)[0][0])  #prediction update

def recursive_mean(res, res_mean, f_factor):
    '''
    :param res: current value of residue
    :param res_mean: past value of recursive mean of the residues
    :param f_factor: forgetting factor
    :return: the value of the recusrive mean of residues for the current sample
    '''
    return f_factor * res_mean[-1] + ((1 - f_factor) * res[-1])

def recursive_var(res, res_mean, var_mean, f_factor):
    '''
    :param res: current value of residue
    :param res_mean: past value of recursive mean of the residues
    :param var_mean: past value of recursive variance of the residues
    :param f_factor: forgetting factor
    :return: the value of the recusrive variance of residues for the current sample
    '''
    first_half = ((2*f_factor-1)/f_factor)*(var_mean[-1])
    second_half = (1-f_factor)*(res[-1] - res_mean[-1])
    return first_half+second_half

def check_string_status(conn, string, irr, p_meas, f_factor, t):
    '''
    :param conn: conn with the POSTGRESQL database
    :param string: number of the current string being computed [0, Np-1]
    :param irr: current irradiance for the inclined pannel
    :param p_meas: measured (synthetic) values of power output
    :param f_factor: forgetting factor
    :param t: timestamp of the sample
    :return: current power output estimate (RLS), residue, recursive mean and variance of the residues,
    fault status flag (True -> fault; False -> normal op)
    '''
    #Read past data to compute the ARX model
    cursor = conn.cursor()
    query_model = '''SELECT * FROM model_data'''
    cursor.execute(query_model)
    model_data = np.array(cursor.fetchall()) #past data on residue dependent stats
    query_synthetic = '''SELECT * FROM synthetic'''
    cursor = conn.cursor()
    cursor.execute(query_synthetic)
    synthetic_data = np.array(cursor.fetchall()) #past data on irradiation and power output

    #Define past data to compute the ARX model
    past_irr = synthetic_data[:,2]
    res_stats = model_data[:,1+int(string*4):int(string*4)+5]
    mean_res = res_stats[:,0] #past recursive mean of the residues
    var_res = res_stats[:,1] #past recursive variance of the residues
    p_hat = res_stats[:,3] #past power output predictions of the model
    curr_p_hat = recursive_LS(irr, past_irr, p_hat, p_meas, string) #estimated power output current sample
    res = p_meas - curr_p_hat #residue
    curr_res_mean = recursive_mean(res, mean_res, f_factor) #recursive mean of the residues
    curr_var_res = recursive_var(res, mean_res, var_res, f_factor) #recursive variance of the residues

    #Adaptive threshold
    if np.abs(res[-1]) > 4 * np.abs(curr_res_mean) and \
            np.abs(curr_var_res) > 6 * np.abs(np.mean(var_res) + 1):
        status = True
    else:
        status = False

    return curr_p_hat, res, curr_res_mean, curr_var_res, status

def fault_label(simu_sample, p_string, status, string, t):
    '''
    :param simu_sample: simulated values for the current sample
    :param p_string: computed power output for the string
    :param status: fault status flag (True -> fault; False -> normal op)
    :param string: number of the current string being computed [0, Np-1]
    :param t: timestamp of the sample
    :return: the label of the fault status
    NO -> normal operation
    DG -> degradation
    OC -> open-circuit
    SC -> short-circuit
    SD -> shading
    '''
    if not status:
        return 'NO' #if the status flag is False: normal operation
    else:
        if p_string == 0:
            #if the power output is zero -> open-circuit
            return 'OC'
        else:
            all_voltage_keys = [key for key in simu_sample if key.startswith(f'v_OT{string}')] #filter only voltage entries
            all_voltages = [simu_sample[key] for key in all_voltage_keys]
            if any(element == float(0) for element in all_voltages):
                #if the voltages list has at least one 0.0 entry is a case of short circuit
                return 'SC'
            else:
                #if the voltages are all the same and the power output is not zero: degradation or shading
                if all(element == all_voltages[0] for element in all_voltages[1:]):
                    #if all the voltages are the same is a case of degradation
                    return 'DG'
                else:
                    #if at least one voltage differs is a case of shading
                    return 'SD'


