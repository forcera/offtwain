import numpy as np
import datetime
from scipy.interpolate import make_interp_spline

def julian_day(day, month):
    '''
        :param day: day of the month [int]
        :param day: month [int]
        :return: given %d.%m date in the julian date equivalent (day of the year)
    '''
    date_format = '%d.%m'
    date = f'{day}.{month}'
    formated = datetime.datetime.strptime(date, date_format)
    date_tuple = formated.timetuple()
    return date_tuple.tm_yday

def eq_time(day):
    '''
        :param day: day of the month [int]
        equation of time, as presented in in the book:
        Solar Engineering of Thermal Processes, Photovoltaics
        and Wind, 5th edition
        by J. A. Duffie, W. A. Beckman and N. Blair
        :return: unit = [min]
    '''
    B_n = (day - 1) * 360 / 365
    E = 229.2 * (0.000075 + 0.001868 * np.cos(np.deg2rad(B_n)) - 0.032077 * np.sin(np.deg2rad(B_n)) -
                    0.014615 * np.cos(2 * np.deg2rad(B_n)) - 0.04089 * np.sin(2 * np.deg2rad(B_n)))
    return E

def solar_time(std_time, GMT, L_loc, N):
    '''
        :param std_time: local solar time [int]
        :param GMT: (+/- GMT time zone)
        :param L_loc: observer's longitude [degrees]
        :param N: julian date [int]
        :return: apparent solar time (in decimal)
    '''
    L_s = 15*GMT
    dS = 4*(L_s - L_loc) + eq_time(N)
    dS /= 60 #percentage of an hour
    return std_time + dS

def declination(N):
    '''
        :param N: julian date [int]
        :return: approximated angular position of the sun at the solar noon [degrees]
    '''
    angle = np.deg2rad(360*((284+N)/365))
    return 23.45*np.sin(angle)

def R_b(phi, beta, delta, time):
    '''
        :param phi: latitude of the observer [degree]
        :param beta: slope of the surface [degree]
        :param delta: declination [degree]
        :param time: time in decimals [float]
        :return: ratio between radiation on an inclined surface and horizontal
    '''
    #converting from degree to rad
    phi = np.deg2rad(phi)
    beta = np.deg2rad(beta)
    delta = np.deg2rad(delta)
    omega = -15*(12 - time)
    omega = np.deg2rad(omega)

    #Rb = cos(theta)/cos(theta_z)
    theta = np.cos(phi-beta)*np.cos(delta)*np.cos(omega) + np.sin(phi-beta)*np.sin(delta)
    theta_z = np.cos(phi)*np.cos(delta)*np.cos(omega) + np.sin(phi)*np.sin(delta)
    return theta/theta_z, theta, theta_z

def R_b_ave(phi, beta, delta, gamma, time1, time2):
    '''
        :param phi: latitude of the observer [degree]
        :param beta: slope of the surface [degree]
        :param delta: declination [degree]
        :param gamma: surface azimuth angle [degree]
        :param time1: initial instant of the interval in decimals [float]
        :param time2: final instant of the interval in decimals [float]
        :return: average of the ratio between radiation on an inclined surface and horizontal
    '''
    # converting from degree to rad
    phi = np.deg2rad(phi)
    beta = np.deg2rad(beta)
    delta = np.deg2rad(delta)
    gamma = np.deg2rad(gamma)
    omega1 = -15 * (12 - time1)
    omega2 = -15 * (12 - time2)

    #R_b_ave = a/b
    a = (np.sin(delta)*np.sin(phi) * np.cos(beta) - np.sin(delta)*np.cos(phi)*np.sin(beta)*np.cos(gamma)) * \
        ((1/180)*(omega2-omega1)*(np.pi)) + \
        (np.cos(delta)*np.cos(phi)*np.cos(beta) + np.cos(delta)*np.sin(phi)*np.sin(beta)*np.cos(gamma)) * \
        (np.sin(np.deg2rad(omega2)) - np.sin(np.deg2rad(omega1))) - \
        (np.cos(delta)*np.sin(beta)*np.sin(gamma)) * (np.cos(np.deg2rad(omega2)) - np.cos(np.deg2rad(omega1)))
    b = (np.cos(phi)*np.cos(delta)) * (np.sin(np.deg2rad(omega2)) - np.sin(np.deg2rad(omega1))) + \
        (np.sin(phi) * np.sin(delta)) * ((1/180)*(omega2-omega1)*(np.pi))
    return a/b

def erbs_correlation(k_t):
    '''
    :param k_t: hour clearness index / cloudiness index [float]
    :return: ratio between total diffuse radiation and total radiation (1 hour period)
    '''
    if k_t <= 0.22:
        return 1 - 0.09*k_t
    elif k_t > 0.22 and k_t <= 0.8:
        return 0.9511 - (0.1604*k_t) + (4.388*(k_t**2)) - (16.638*(k_t**3)) + (12.336*(k_t**4))
    else:
        return 0.165

def albedo(c, r, N, time, phi):
    '''
    :param c: colour coefficient [float] - K. Trapani, 2014
    :param r: roughness coefficient [float] - K. Trapani, 2014
    :param N: julian date [int]
    :param time: time in decimals [float]
    :param phi: latitude of the observer [degrees]
    :return: estimation of the albedo (diffuse reflectance of the surface) [float]
    '''
    # converting from degree to rad
    delta = declination(N)
    delta = np.deg2rad(delta)
    omega = -15*(12-time)
    omega = np.deg2rad(omega)
    phi = np.deg2rad(phi)

    #calculating the solar altitude angle
    theta_z = np.cos(phi)*np.cos(delta)*np.cos(omega) + np.sin(phi)*np.sin(delta)
    alpha_s = np.arcsin(theta_z) #cos(theta_z) = sin(alpha_s)

    #estimating the albedo of a water surface
    return c**(r*np.sin(alpha_s) + 1)

def altitude(phi, N, time):
    '''
    :param phi: latitude of the observer [degrees]
    :param N: julian date [int]
    :param time: time in decimals [float]
    :return: solar altitude angle
    '''
    # converting from degree to rad
    if time < 12.5 and time > 0:
        time -= 2
    elif time >= 12.5 and time <= 18:
        time += 1

    delta = declination(N)
    delta = np.deg2rad(delta)
    omega = -15 * (12 - time)
    omega = np.deg2rad(omega)
    phi = np.deg2rad(phi)

    # calculating the solar altitude angle
    theta_z = np.cos(phi) * np.cos(delta) * np.cos(omega) + np.sin(phi) * np.sin(delta)
    alpha_s = np.arcsin(theta_z)  # cos(theta_z) = sin(alpha_s)
    return np.rad2deg(alpha_s)

def incidence(beta, phi, N, time):
    '''
    :param beta: slope of the surface [degrees]
    :param phi: latitude of the observer [degrees]
    :param N: julian date [int]
    :param time: time in decimals [float]
    :return: incidence of the solar rays on the surface [degrees]
    '''
    # converting from degree to rad
    delta = declination(N)
    delta = np.deg2rad(delta)
    omega = -15 * (12 - time)
    omega = np.deg2rad(omega)
    phi = np.deg2rad(phi)

    #calculating the incidence of the solar rays
    theta = np.cos(phi-beta)*np.cos(delta)*np.cos(omega) + np.sin(phi-beta)*np.sin(delta)
    return np.rad2deg(theta)

def azimuth(phi, N, time):
    '''
    :param phi: latitude of the observer [degrees]
    :param N: julian date [int]
    :param time: time in decimals [float]
    :return: solar azimuth angle
    '''
    #converting from degree to rad
    delta = declination(N)
    delta = np.deg2rad(delta)
    omega = -15 * (12 - time)
    omega = np.deg2rad(omega)
    phi = np.deg2rad(phi)

    #calculating the zenith angle
    theta_z = np.cos(phi) * np.cos(delta) * np.cos(omega) + np.sin(phi) * np.sin(delta)
    theta_z = np.arccos(theta_z)

    #azimuth computations
    num = np.cos(theta_z)*np.sin(phi) - np.sin(delta)
    den = np.sin(theta_z)*np.cos(phi)
    abs = np.abs(np.arccos(num/den))
    gamma = np.sign(omega)*abs
    return np.rad2deg(gamma)

def isotropic(day, month, time1, time2, beta, gamma, phi, rho_g, I):
    '''
        :param day: day of the month [int]
        :param month: month [int]
        :param time1: initial instant of the interval in decimals [float]
        :param time2: final instant of the interval in decimals [float]
        :param beta: slope of the surface [degree]
        :param gamma: surface azimuth angle [degree]
        :param phi: latitude of the observer [degree]
        :param rho: diffuse reflectance of the surface (albedo) [float]
        :param I: irradiation for an hour time period at a horizontal surface
        :return: estimation of the irradiation for an hour time period at the inclined surface
    '''
    #Necessary prior calculations
    N = julian_day(day, month)
    delta = declination(N)

    # compensating for sunrise/sundown
    sun_presence = -np.tan(np.deg2rad(phi)) * np.tan(np.deg2rad(delta))
    omega_s = np.rad2deg(np.arccos(sun_presence))
    time_r_d = 12 - omega_s / 15
    if time1 < time_r_d and time2 > time_r_d:
        time1 = time_r_d
    elif time2 > (omega_s/15) + 12 and time1 < (omega_s/15) + 12:
        time2 = time_r_d + 12
    else:
        time1 = time1
        time2 = time2
    omega1 = -15*(12-time1)
    omega2 = -15*(12-time2)

    #Yielding the direct and diffuse irradiances
    I_o_part1 = 1 + (0.033 * np.cos(np.deg2rad((360*N)/365)))
    I_o_part2 = (np.pi * (omega2 - omega1))/180
    I_o = (12*3600/np.pi) * (1367*I_o_part1) * (np.cos(np.deg2rad(phi)) * np.cos(np.deg2rad(delta)) *
          (np.sin(np.deg2rad(omega2)) - np.sin(np.deg2rad(omega1))) +
          I_o_part2 * np.sin(np.deg2rad(phi)) * np.sin(np.deg2rad(delta))) #extra-terrestrial radiation
    k_t = I/I_o #cloudiness index
    r_Id_I = erbs_correlation(k_t) #r = diffuse/total
    I_d = I * r_Id_I #diffuse irradiance
    I_b = I * (1 - r_Id_I) #direct incidence
    R, a_theta, a_theta_z = R_b(phi,beta, delta, np.mean([time1, time2])) #radiation ratio
    if R > 20:
        R = R_b_ave(phi, beta, delta, gamma, time1, time2) #if R >>: Ib*Rb might be greater than the sun constant

    #Calculating the irradiances (direct and diffuse)
    direct = I_b * R #direct radiation on the tilted surface
    diff_sky = I_d * ((1 + np.cos(np.deg2rad(beta)))/2) #diffuse radiation from the atmosphere
    diff_ground = I * rho_g * ((1 - np.cos(np.deg2rad(beta)))/2)
    return direct, diff_sky, diff_ground

def brightness_coeff(epsilon): #perez et al. 1990 values
    if 1 <= epsilon and epsilon < 1.065:
        return np.array([[-0.008, 0.588, -0.062],[-0.06, 0.072, -0.022]])
    elif 1.065 <= epsilon and epsilon < 1.23:
        return np.array([[0.130, 0.683, -0.151],[-0.019, 0.066, -0.029]])
    elif 1.23 <= epsilon and epsilon < 1.5:
        return np.array([[0.33, 0.487, -0.221],[0.055, -0.064, -0.026]])
    elif 1.5 <= epsilon and epsilon < 1.95:
        return np.array([[0.568, 0.187, -0.295],[0.109, -0.152, -0.014]])
    elif 1.95 <= epsilon and epsilon < 2.8:
        return np.array([[0.873, -0.392, -0.362],[0.226, -0.462, 0.001]])
    elif 2.8 <= epsilon and epsilon < 4.5:
        return np.array([[1.132, -1.237, -0.412],[0.288, -0.823, 0.056]])
    elif 4.5 <= epsilon and epsilon < 6.2:
        return np.array([[1.06, -1.6, -0.359],[0.264, -1.127, 0.131]])
    else:
        return np.array([[0.678, -0.327, -0.250], [0.156, -1.377, 0.251]])

def perez(day, month, time1, time2, beta, gamma, phi, rho_g, I):
    '''
        :param day: day of the month [int]
        :param month: month [int]
        :param time1: initial instant of the interval in decimals [float]
        :param time2: final instant of the interval in decimals [float]
        :param beta: slope of the surface [degree]
        :param gamma: surface azimuth angle [degree]
        :param phi: latitude of the observer [degree]
        :param rho: diffuse reflectance of the surface (albedo) [float]
        :param I: irradiation for an hour time period at a horizontal surface
        :return: estimation of the irradiation for an hour time period at the inclined surface
    '''
    # Necessary prior calculations
    N = julian_day(day, month)
    delta = declination(N)

    # compensating for sunrise/sundown
    sun_presence = -np.tan(np.deg2rad(phi)) * np.tan(np.deg2rad(delta))
    omega_s = np.rad2deg(np.arccos(sun_presence))
    time_r_d = 12 - omega_s / 15
    if time1 < time_r_d and time2 > time_r_d:
        time1 = time_r_d
    elif time2 > (omega_s / 15) + 12 and time1 < (omega_s / 15) + 12:
        time2 = time_r_d + 12
        
    else:
        time1 = time1
        time2 = time2
    omega1 = -15 * (12 - time1)
    omega2 = -15 * (12 - time2)

    # Yielding the direct and diffuse irradiances
    I_o_part1 = 1 + (0.033 * np.cos(np.deg2rad((360 * N) / 365)))
    I_o_part2 = (np.pi * (omega2 - omega1)) / 180
    I_o = (12 * 3600 / np.pi) * (1367 * I_o_part1) * (np.cos(np.deg2rad(phi)) * np.cos(np.deg2rad(delta)) *
          (np.sin(np.deg2rad(omega2)) - np.sin(np.deg2rad(omega1))) +
          I_o_part2 * np.sin(np.deg2rad(phi)) * np.sin(np.deg2rad(delta)))  # extra-terrestrial radiation
    k_t = I / I_o  # cloudiness index
    r_Id_I = erbs_correlation(k_t)  # r = diffuse/total
    I_d = I * r_Id_I  # diffuse irradiance
    I_b = I * (1 - r_Id_I)  # direct incidence
    R, a_theta, a_theta_z = R_b(phi, beta, delta, np.mean([time1, time2]))  # radiation ratio
    if R > 20:
        R = R_b_ave(phi, beta, delta, gamma, time1, time2)  # if R >>: Ib*Rb might be greater than the sun constant

    #Calculating normal incidences
    I_o_cos_n = np.cos(np.deg2rad((360 * N) / 365))
    I_o_n = (1367*3600) * (1 + (0.033 * I_o_cos_n)) #normal extreterrestrial incidence
    I_b_n = I_b/a_theta_z #normal direct incidence

    #Perez et al. 1990 prior calculations
    #a = np.max([0, a_theta[0]]) #brightness coeff. a
    a = np.max([0, a_theta])  # brightness coeff. a
    b = np.max([np.cos(np.deg2rad(85)), a_theta_z]) #brightness coeff. b
    m = 1 / a_theta_z #air mass
    bright = m * (I_d / I_o_n) #brightness (Delta)
    epsilon_div = (I_d + I_b_n) / I_d
    theta_cube = 5.535 * (10**-6) * (np.rad2deg(np.arccos(a_theta_z))**3)
    epsilon = (epsilon_div + theta_cube) / (1 + theta_cube) #clearness
    f = brightness_coeff(epsilon) #brightness coefficients
    conv_theta_z = (np.pi * np.rad2deg(np.arccos(a_theta_z)))/180
    F1_sum = f[0,0] + (f[0,1] * bright) + (conv_theta_z * f[0,2])
    F1 = np.max([0, F1_sum]) #brightness coeff. F1
    F2 = f[1,0] + (f[1,1] * bright) + (conv_theta_z * f[1,2])

    # Calculating the irradiances (direct and diffuse)
    direct = I_b * R #direct radiation on the tilted surface
    diff_brightening = (I_d*(1-F1)*((1+np.cos(np.deg2rad(beta)))/2))+(I_d*F1*(a/b))+(I_d*F2*np.sin(np.deg2rad(beta)))
    diff_ground = I * rho_g * ((1 - np.cos(np.deg2rad(beta)))/2)
    return direct, diff_brightening, diff_ground

def HDKR(day, month, time1, time2, beta, gamma, phi, rho_g, I):
    '''
        :param day: day of the month [int]
        :param month: month [int]
        :param time1: initial instant of the interval in decimals [float]
        :param time2: final instant of the interval in decimals [float]
        :param beta: slope of the surface [degree]
        :param gamma: surface azimuth angle [degree]
        :param phi: latitude of the observer [degree]
        :param rho: diffuse reflectance of the surface (albedo) [float]
        :param I: irradiation for an hour time period at a horizontal surface
        :return: estimation of the irradiation for an hour time period at the inclined surface
    '''
    #Necessary prior calculations
    N = julian_day(day, month)
    delta = declination(N)

    # compensating for sunrise/sundown
    sun_presence = -np.tan(np.deg2rad(phi)) * np.tan(np.deg2rad(delta))
    omega_s = np.rad2deg(np.arccos(sun_presence))
    time_r_d = 12 - omega_s / 15
    if time1 < time_r_d and time2 > time_r_d:
        time1 = time_r_d
    elif time2 > (omega_s/15) + 12 and time1 < (omega_s/15) + 12:
        time2 = time_r_d + 12
    else:
        time1 = time1
        time2 = time2
    omega1 = -15*(12-time1)
    omega2 = -15*(12-time2)

    #Yielding the direct and diffuse irradiances
    I_o_part1 = 1 + (0.033 * np.cos(np.deg2rad((360*N)/365)))
    I_o_part2 = (np.pi * (omega2 - omega1))/180
    I_o = (12*3600/np.pi) * (1367*I_o_part1) * (np.cos(np.deg2rad(phi)) * np.cos(np.deg2rad(delta)) *
          (np.sin(np.deg2rad(omega2)) - np.sin(np.deg2rad(omega1))) +
          I_o_part2 * np.sin(np.deg2rad(phi)) * np.sin(np.deg2rad(delta))) #extra-terrestrial radiation
    k_t = I/I_o #cloudiness index
    r_Id_I = erbs_correlation(k_t) #r = diffuse/total
    I_d = I * r_Id_I #diffuse irradiance
    I_b = I * (1 - r_Id_I) #direct incidence
    R, a_theta, a_theta_z = R_b(phi,beta, delta, np.mean([time1, time2])) #radiation ratio
    if R > 20:
        R = R_b_ave(phi, beta, delta, gamma, time1, time2) #if R >>: Ib*Rb might be greater than the sun constant

    #Reindl et al. 1990 prior calculations
    Ai = I_b / I_o #anisotropy index
    f = np.sqrt(I_b / I) #modulator factor
    acc_cloud = 1 + (f * ((np.sin(np.deg2rad(beta/2)))**3)) #accounting for horizon brightness in a cloudy sky

    # Calculating the irradiances (direct and diffuse)
    direct = I_b * R  # direct radiation on the tilted surface
    diff_cloudy = (I_d * Ai * R) + (I_d * (1 - Ai) * ((1 + np.cos(np.deg2rad(beta))) / 2) * acc_cloud)
    diff_ground = I * rho_g * ((1 - np.cos(np.deg2rad(beta))) / 2)
    return direct, diff_cloudy, diff_ground

def plot_interp(time, output, convert=False):
    if convert:
        output = [rad / 3600 for rad in output]
    t_G_spline = make_interp_spline(time, output)
    time_res = np.linspace(time.min(), time.max(), 1320)
    output_res = t_G_spline(time_res)
    return time_res, output_res
