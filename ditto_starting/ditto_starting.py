import subprocess
import json
import time
import os

policy_command = [
    "curl",
    "-X", "PUT",
    "http://ditto-nginx-1:80/api/2/policies/my.test:policy",
    "-u", "ditto:ditto",
    "-H", "Content-Type: application/json",
    "-d", "@policy.json"
]

sensor_command = [
    "curl",
    "-X", "PUT",
    "http://ditto-nginx-1:80/api/2/things/weather.sensor:01",
    "-u", "ditto:ditto",
    "-H", "Content-Type: application/json",
    "-d", "@sensor.json"
]

with open('connection_source.json', 'r') as file:
    connection_data = json.load(file)

# Modifica o valor de uri
connection_data['piggybackCommand']['connection']['uri'] = f"tcp://{os.getenv('mosquitto_username')}:{os.getenv('mosquitto_password')}@ditto-mosquitto-1:1883"

# Salva as modificações de volta para connection_source.json
with open('connection_source.json', 'w') as file:
    json.dump(connection_data, file, indent=4)

connection_command = [
    "curl",
    "-X", "POST",
    "http://devops:foobar@ditto-gateway-1:8080/devops/piggyback/connectivity?timeout=10",
    "-H", "Content-Type: application/json",
    "-d", "@connection_source.json"
]

with open('panel_connection.json', 'r') as file:
    connection_data = json.load(file)

# Modifica o valor de uri
connection_data['piggybackCommand']['connection']['uri'] = f"tcp://{os.getenv('mosquitto_username')}:{os.getenv('mosquitto_password')}@ditto-mosquitto-1:1883"

# Salva as modificações de volta para panel_connection.json
with open('panel_connection.json', 'w') as file:
    json.dump(connection_data, file, indent=4)

panel_connection_command = [
    "curl",
    "-X", "POST",
    "http://devops:foobar@ditto-gateway-1:8080/devops/piggyback/connectivity?timeout=10",
    "-H", "Content-Type: application/json",
    "-d", "@panel_connection.json"
]

panel = {
    "policyId": "my.test:policy",
    "attributes": {
        "panel": "F1-S1-P1",
        "series": "F1-S1",
        "field": "F1"
    },
    "features": {
        "measurements": {
            "properties": {
                "time": 0,
                "inclination": 0,
                "irradiance": 0,
                "i_dc": 0,
                "v_dc": 0,
                "p_dc": 0
            }
        }
    }
}

while True:
    try:
        response = subprocess.run(policy_command, check=True, stdout=subprocess.PIPE)
        if str(response.stdout)[2:8] != "<html>":
            break
    except subprocess.CalledProcessError as e:
        print("Error executing the command:", e)
    time.sleep(1)

try:
    subprocess.run(sensor_command, check=True)
    print("Sensor command executed successfully")
except subprocess.CalledProcessError as e:
    print("Error executing the sensor command:", e)

try:
    subprocess.run(connection_command, check=True)
    print("Connectivity command executed successfully")
except subprocess.CalledProcessError as e:
    print("Error executing the connectivity command:", e)

try:
    subprocess.run(panel_connection_command, check=True)
    print("Connectivity command executed successfully")
except subprocess.CalledProcessError as e:
    print("Error executing the connectivity command:", e)


for series in range(3):
    for paineis in range(8):
        panel['attributes'] = {
            "panel": f"F1-S{(series)}-P{paineis}",
            "series": f"F1-S{series}",
            "field": f"F1"
        }
        panel_json = json.dumps(panel)
        print(panel_json)
        panel_command = [
            "curl",
            "-X", "PUT",
            f"http://ditto-nginx-1:80/api/2/things/offtwAIn_panel:{panel['attributes']['panel']}",
            "-u", "ditto:ditto",
            "-H", "Content-Type: application/json",
            "-d", panel_json
        ]
        try:
            response = subprocess.run(panel_command, check=True, stdout=subprocess.PIPE)

            print(response.stdout)
        except subprocess.CalledProcessError as e:
            print("Error executing the command:", e)