# Starting Ditto

To start diddo execution use the following commands

```
cd ditto
docker-compose up -d
```

### Adding policy for things

```
curl -X PUT 'http://localhost:8080/api/2/policies/my.test:policy' -u 'ditto:ditto' -H 'Content-Type: application/json' -d @ditto_content/policy.json
```
### Adding Things

```
curl -X PUT 'http://localhost:8080/api/2/things/weather.sensor:01' -u 'ditto:ditto' -H 'Content-Type: application/json' -d @ditto_content/sensor.json
```

### Creating mqtt connection

```
curl -X POST 'http://devops:foobar@localhost:8080/devops/piggyback/connectivity?timeout=10' -H 'Content-Type: application/json' -d @ditto_content/connection_source.json
```

### Sending MQTT message to upload a panel data

```
mosquitto_pub -h localhost -m '{"temperature": 25, "humidity": 60, "thingId": "weather.sensor:01"}' -t weather.sensor/01
```


curl -X PUT 'http://localhost:8080/api/2/things/panel:F1-S1-P1' -u 'ditto:ditto' -H 'Content-Type: application/json' -d @ditto_content/panel.json