CREATE TABLE panels (
    time        TIMESTAMPTZ     NOT NULL,
    panel       TEXT            NOT NULL,
    series      TEXT            NOT NULL,
    inclination DOUBLE PRECISION  NOT NULL,
    irradiance  DOUBLE PRECISION  NOT NULL,
    i_dc        DOUBLE PRECISION  NOT NULL,
    v_dc        DOUBLE PRECISION  NOT NULL,
    p_dc        DOUBLE PRECISION  NOT NULL,
    PRIMARY KEY (panel, time)
);

SELECT create_hypertable('panels', 'time');

CREATE TABLE weather (
    time            TIMESTAMPTZ       NOT NULL,
    temperature         DOUBLE PRECISION  NOT NULL,
    humidity            DOUBLE PRECISION  NOT NULL,
    dew_point           DOUBLE PRECISION  NOT NULL,
    windspeed           DOUBLE PRECISION  NOT NULL,
    winddirection       DOUBLE PRECISION  NOT NULL,
    cloudcover          DOUBLE PRECISION  NOT NULL,
    latitude            DOUBLE PRECISION  NOT NULL,
    longitude           DOUBLE PRECISION  NOT NULL,


    PRIMARY KEY (latitude, longitude, time)
);

SELECT create_hypertable('weather', 'time');

CREATE TABLE synthetic (
    time            TIMESTAMPTZ       NOT NULL,
    inclination     DOUBLE PRECISION  NOT NULL,
    irradiance      DOUBLE PRECISION  NOT NULL,
    i_OT00_dc       DOUBLE PRECISION  NOT NULL,
    v_OT00_dc       DOUBLE PRECISION  NOT NULL,
    fault_OT00      SMALLINT          NOT NULL,
    i_OT01_dc       DOUBLE PRECISION  NOT NULL,
    v_OT01_dc       DOUBLE PRECISION  NOT NULL,
    fault_OT01      SMALLINT          NOT NULL,
    i_OT02_dc       DOUBLE PRECISION  NOT NULL,
    v_OT02_dc       DOUBLE PRECISION  NOT NULL,
    fault_OT02      SMALLINT          NOT NULL,
    i_OT03_dc       DOUBLE PRECISION  NOT NULL,
    v_OT03_dc       DOUBLE PRECISION  NOT NULL,
    fault_OT03      SMALLINT          NOT NULL,
    i_OT04_dc       DOUBLE PRECISION  NOT NULL,
    v_OT04_dc       DOUBLE PRECISION  NOT NULL,
    fault_OT04      SMALLINT          NOT NULL,
    i_OT05_dc       DOUBLE PRECISION  NOT NULL,
    v_OT05_dc       DOUBLE PRECISION  NOT NULL,
    fault_OT05      SMALLINT          NOT NULL,
    i_OT06_dc       DOUBLE PRECISION  NOT NULL,
    v_OT06_dc       DOUBLE PRECISION  NOT NULL,
    fault_OT06      SMALLINT          NOT NULL,
    i_OT07_dc       DOUBLE PRECISION  NOT NULL,
    v_OT07_dc       DOUBLE PRECISION  NOT NULL,
    fault_OT07      SMALLINT          NOT NULL,
    p_OT0_dc        DOUBLE PRECISION  NOT NULL,
    status_OT0      BOOLEAN           NOT NULL,
    i_OT10_dc       DOUBLE PRECISION  NOT NULL,
    v_OT10_dc       DOUBLE PRECISION  NOT NULL,
    fault_OT10      SMALLINT          NOT NULL,
    i_OT11_dc       DOUBLE PRECISION  NOT NULL,
    v_OT11_dc       DOUBLE PRECISION  NOT NULL,
    fault_OT11      SMALLINT          NOT NULL,
    i_OT12_dc       DOUBLE PRECISION  NOT NULL,
    v_OT12_dc       DOUBLE PRECISION  NOT NULL,
    fault_OT12      SMALLINT          NOT NULL,
    i_OT13_dc       DOUBLE PRECISION  NOT NULL,
    v_OT13_dc       DOUBLE PRECISION  NOT NULL,
    fault_OT13      SMALLINT          NOT NULL,
    i_OT14_dc       DOUBLE PRECISION  NOT NULL,
    v_OT14_dc       DOUBLE PRECISION  NOT NULL,
    fault_OT14      SMALLINT          NOT NULL,
    i_OT15_dc       DOUBLE PRECISION  NOT NULL,
    v_OT15_dc       DOUBLE PRECISION  NOT NULL,
    fault_OT15      SMALLINT          NOT NULL,
    i_OT16_dc       DOUBLE PRECISION  NOT NULL,
    v_OT16_dc       DOUBLE PRECISION  NOT NULL,
    fault_OT16      SMALLINT          NOT NULL,
    i_OT17_dc       DOUBLE PRECISION  NOT NULL,
    v_OT17_dc       DOUBLE PRECISION  NOT NULL,
    fault_OT17      SMALLINT          NOT NULL,
    p_OT1_dc        DOUBLE PRECISION  NOT NULL,
    status_OT1      BOOLEAN           NOT NULL,
    i_OT20_dc       DOUBLE PRECISION  NOT NULL,
    v_OT20_dc       DOUBLE PRECISION  NOT NULL,
    fault_OT20      SMALLINT          NOT NULL,
    i_OT21_dc       DOUBLE PRECISION  NOT NULL,
    v_OT21_dc       DOUBLE PRECISION  NOT NULL,
    fault_OT21      SMALLINT          NOT NULL,
    i_OT22_dc       DOUBLE PRECISION  NOT NULL,
    v_OT22_dc       DOUBLE PRECISION  NOT NULL,
    fault_OT22      SMALLINT          NOT NULL,
    i_OT23_dc       DOUBLE PRECISION  NOT NULL,
    v_OT23_dc       DOUBLE PRECISION  NOT NULL,
    fault_OT23      SMALLINT          NOT NULL,
    i_OT24_dc       DOUBLE PRECISION  NOT NULL,
    v_OT24_dc       DOUBLE PRECISION  NOT NULL,
    fault_OT24      SMALLINT          NOT NULL,
    i_OT25_dc       DOUBLE PRECISION  NOT NULL,
    v_OT25_dc       DOUBLE PRECISION  NOT NULL,
    fault_OT25      SMALLINT          NOT NULL,
    i_OT26_dc       DOUBLE PRECISION  NOT NULL,
    v_OT26_dc       DOUBLE PRECISION  NOT NULL,
    fault_OT26      SMALLINT          NOT NULL,
    i_OT27_dc       DOUBLE PRECISION  NOT NULL,
    v_OT27_dc       DOUBLE PRECISION  NOT NULL,
    fault_OT27      SMALLINT          NOT NULL,
    p_OT2_dc        DOUBLE PRECISION  NOT NULL,
    status_OT2      BOOLEAN           NOT NULL,
    PRIMARY KEY (time)
);
SELECT create_hypertable('synthetic', 'time');


CREATE TABLE model_data (
    time                   TIMESTAMPTZ       NOT NULL,
    mean_residue_OT0       DOUBLE PRECISION  NOT NULL,
    var_residue_OT0        DOUBLE PRECISION  NOT NULL,
    residue_OT0            DOUBLE PRECISION  NOT NULL,
    p_hat_OT0              DOUBLE PRECISION  NOT NULL,
    mean_residue_OT1       DOUBLE PRECISION  NOT NULL,
    var_residue_OT1        DOUBLE PRECISION  NOT NULL,
    residue_OT1            DOUBLE PRECISION  NOT NULL,
    p_hat_OT1              DOUBLE PRECISION  NOT NULL,
    mean_residue_OT2       DOUBLE PRECISION  NOT NULL,
    var_residue_OT2        DOUBLE PRECISION  NOT NULL,
    residue_OT2            DOUBLE PRECISION  NOT NULL,
    p_hat_OT2              DOUBLE PRECISION  NOT NULL,
    
    
    PRIMARY KEY (time)
);

SELECT create_hypertable('model_data', 'time');