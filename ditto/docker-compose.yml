version: '2.4'

services:
  mongodb:
    image: docker.io/mongo:5.0
    mem_limit: 256m
    restart: always
    networks:
      default:
        aliases:
          - mongodb
    command: mongod --storageEngine wiredTiger --noscripting
    user: mongodb
    ports:
      - 27017:27017
    environment:
       - TZ=${TZ:-Europe/Lisbon}

  policies:
    image: docker.io/eclipse/ditto-policies:${DITTO_VERSION:-latest}
    mem_limit: 512m
    restart: always
    networks:
      default:
        aliases:
          - ditto-cluster
    environment:
      - TZ=${TZ:-Europe/Lisbon}
      - BIND_HOSTNAME=0.0.0.0
      - JAVA_TOOL_OPTIONS=-XX:ActiveProcessorCount=2 -XX:+ExitOnOutOfMemoryError -XX:+UseContainerSupport -XX:+UseStringDeduplication -Xss512k -XX:MaxRAMPercentage=50 -XX:+UseG1GC -XX:MaxGCPauseMillis=150 -Dpekko.coordinated-shutdown.exit-jvm=on -Dpekko.cluster.shutdown-after-unsuccessful-join-seed-nodes=180s -Dpekko.cluster.failure-detector.threshold=15.0 -Dpekko.cluster.failure-detector.expected-response-after=10s -Dpekko.cluster.failure-detector.acceptable-heartbeat-pause=20s -Dpekko.cluster.downing-provider-class=
      - MONGO_DB_HOSTNAME=mongodb
    healthcheck:
      test: curl --fail `hostname`:7626/alive || exit 1
      interval: 30s
      timeout: 15s
      retries: 4
      start_period: 120s

  things:
    image: docker.io/eclipse/ditto-things:${DITTO_VERSION:-latest}
    mem_limit: 512m
    restart: always
    networks:
      default:
        aliases:
          - ditto-cluster
    depends_on:
      - policies
    environment:
      - TZ=${TZ:-Europe/Lisbon}
      - BIND_HOSTNAME=0.0.0.0
      - JAVA_TOOL_OPTIONS=-XX:ActiveProcessorCount=2 -XX:+ExitOnOutOfMemoryError -XX:+UseContainerSupport -XX:+UseStringDeduplication -Xss512k -XX:MaxRAMPercentage=50 -XX:+UseG1GC -XX:MaxGCPauseMillis=150 -Dpekko.coordinated-shutdown.exit-jvm=on -Dpekko.cluster.shutdown-after-unsuccessful-join-seed-nodes=180s -Dpekko.cluster.failure-detector.threshold=15.0 -Dpekko.cluster.failure-detector.expected-response-after=10s -Dpekko.cluster.failure-detector.acceptable-heartbeat-pause=20s -Dpekko.cluster.downing-provider-class=
      - MONGO_DB_HOSTNAME=mongodb
    healthcheck:
      test: curl --fail `hostname`:7626/alive || exit 1
      interval: 30s
      timeout: 15s
      retries: 4
      start_period: 120s

  things-search:
    image: docker.io/eclipse/ditto-things-search:${DITTO_VERSION:-latest}
    mem_limit: 512m
    restart: always
    networks:
      default:
        aliases:
          - ditto-cluster
    depends_on:
      - policies
    environment:
      - TZ=${TZ:-Europe/Lisbon}
      - BIND_HOSTNAME=0.0.0.0
      - JAVA_TOOL_OPTIONS=-XX:ActiveProcessorCount=2 -XX:+ExitOnOutOfMemoryError -XX:+UseContainerSupport -XX:+UseStringDeduplication -Xss512k -XX:MaxRAMPercentage=50 -XX:+UseG1GC -XX:MaxGCPauseMillis=150 -Dpekko.coordinated-shutdown.exit-jvm=on -Dpekko.cluster.shutdown-after-unsuccessful-join-seed-nodes=180s -Dpekko.cluster.failure-detector.threshold=15.0 -Dpekko.cluster.failure-detector.expected-response-after=10s -Dpekko.cluster.failure-detector.acceptable-heartbeat-pause=20s -Dpekko.cluster.downing-provider-class=
      - MONGO_DB_HOSTNAME=mongodb
    healthcheck:
      test: curl --fail `hostname`:7626/alive || exit 1
      interval: 30s
      timeout: 15s
      retries: 4
      start_period: 120s

  connectivity:
    image: docker.io/eclipse/ditto-connectivity:${DITTO_VERSION:-latest}
    mem_limit: 768m
    restart: always
    networks:
      default:
        aliases:
          - ditto-cluster
    depends_on:
      - policies
    environment:
      - TZ=${TZ:-Europe/Lisbon}
      - BIND_HOSTNAME=0.0.0.0
      - JAVA_TOOL_OPTIONS=-XX:ActiveProcessorCount=2 -XX:+ExitOnOutOfMemoryError -XX:+UseContainerSupport -XX:+UseStringDeduplication -Xss512k -XX:MaxRAMPercentage=50 -XX:+UseG1GC -XX:MaxGCPauseMillis=150 -Dpekko.coordinated-shutdown.exit-jvm=on -Dpekko.cluster.shutdown-after-unsuccessful-join-seed-nodes=180s -Dpekko.cluster.failure-detector.threshold=15.0 -Dpekko.cluster.failure-detector.expected-response-after=10s -Dpekko.cluster.failure-detector.acceptable-heartbeat-pause=20s -Dpekko.cluster.downing-provider-class=
      - MONGO_DB_HOSTNAME=mongodb
    healthcheck:
      test: curl --fail `hostname`:7626/alive || exit 1
      interval: 30s
      timeout: 15s
      retries: 4
      start_period: 120s

  gateway:
    image: docker.io/eclipse/ditto-gateway:${DITTO_VERSION:-latest}
    mem_limit: 512m
    restart: always
    networks:
      default:
        aliases:
          - ditto-cluster
    depends_on:
      - policies
    ports:
      - "8081:8080"
    environment:
      - TZ=${TZ:-Europe/Lisbon}
      - BIND_HOSTNAME=0.0.0.0
      - ENABLE_PRE_AUTHENTICATION=true
      - JAVA_TOOL_OPTIONS=-XX:ActiveProcessorCount=2 -XX:+ExitOnOutOfMemoryError -XX:+UseContainerSupport -XX:+UseStringDeduplication -Xss512k -XX:MaxRAMPercentage=50 -XX:+UseG1GC -XX:MaxGCPauseMillis=150 -Dpekko.coordinated-shutdown.exit-jvm=on -Dpekko.cluster.shutdown-after-unsuccessful-join-seed-nodes=180s -Dpekko.cluster.failure-detector.threshold=15.0 -Dpekko.cluster.failure-detector.expected-response-after=10s -Dpekko.cluster.failure-detector.acceptable-heartbeat-pause=20s -Dpekko.cluster.downing-provider-class=
    healthcheck:
      test: curl --fail `hostname`:7626/alive || exit 1
      interval: 30s
      timeout: 15s
      retries: 4
      start_period: 120s

  ditto-ui:
    image: docker.io/eclipse/ditto-ui:${DITTO_VERSION:-latest}
    mem_limit: 32m
    restart: always

  swagger-ui:
    image: docker.io/swaggerapi/swagger-ui:v5.9.1
    mem_limit: 32m
    restart: always
    environment:
      - QUERY_CONFIG_ENABLED=true
    volumes:
       - ./ditto_files/swagger3-index.html:/usr/share/nginx/html/index.html:ro
    command: nginx -g 'daemon off;'

  nginx:
    image: docker.io/nginx:1.21-alpine
    mem_limit: 32m
    restart: always
    volumes:
       - ./ditto_files/nginx.conf:/etc/nginx/nginx.conf:ro
       - ./ditto_files/nginx.htpasswd:/etc/nginx/nginx.htpasswd:ro
       - ./ditto_files/nginx-cors.conf:/etc/nginx/nginx-cors.conf:ro
       - ./ditto_files/mime.types:/etc/nginx/mime.types:ro
       - ./ditto_files/index.html:/etc/nginx/html/index.html:ro
    ports:
      - "${DITTO_EXTERNAL_PORT:-8080}:80"
    depends_on:
      - gateway
      - swagger-ui

  mosquitto:
    image: eclipse-mosquitto
    ports:
      - "1883:1883"
      - 9001:9001
    environment:
      - TZ=${TZ:-Europe/Lisbon}
    volumes:
      - ./mosquitto/config:/mosquitto/config
      - ./mosquitto/config:/mosquitto/data
      - ./mosquitto/config:/mosquitto/log

  weather_data_collector:
    image: ghcr.io/forcera/weather_data_collector:latest
    environment:
      - TZ=${TZ:-Europe/Lisbon}
      - mosquitto_username=${mosquitto_username:-default_admin_user}
      - mosquitto_password=${mosquitto_password:-default_admin_password}
      - API_TOKEN=${API_TOKEN:-default_API_TOKEN}
    depends_on:
      - mosquitto
      - timescaledb
  
  ditto_starting:
    image: ghcr.io/forcera/ditto_starting:latest
    environment:
      - mosquitto_username=${mosquitto_username:-default_admin_user}
      - mosquitto_password=${mosquitto_password:-default_admin_password}

  timescaledb:
    image: timescale/timescaledb:latest-pg14
    env_file: 
      - .env
    environment:
      - TZ=${TZ:-Europe/Lisbon}
      - POSTGRES_PASSWORD=${POSTGRES_PASSWORD:-default_admin_password}
      - POSTGRES_USER=${POSTGRES_USER:-default_admin_user}
      - POSTGRES_DB=${POSTGRES_DB:-default_database}
    ports:
      - "5432:5432"
    volumes:
      - ./database/initialize.sql:/docker-entrypoint-initdb.d/initialize.sql
      - ./database/data/postgresql:/var/lib/postgresql
      - ./database/data/postgresql/data:/var/lib/postgresql/data
      - ./database/data/timescale:/var/lib/timescale
      - ./database/data/timescale/data:/var/lib/timescale/data

  grafana:
    image: grafana/grafana-enterprise
    user: "root"
    ports:
      - "3000:3000"
    environment:
      - TZ=${TZ:-Europe/Lisbon}
      - GF_SECURITY_ADMIN_USER=${GRAFANA_USER:-default_admin_user}
      - GF_SECURITY_ADMIN_PASSWORD=${GRAFANA_PASSWORD:-default_admin_password}
    volumes:
       - ./grafana/data/dashboards:/etc/grafana/provisioning/dashboards
       - ./grafana/data/datasources.yml:/etc/grafana/provisioning/datasources/datasources.yml
    depends_on:
      - timescaledb
  
  nifi:
    image: apache/nifi:latest
    restart: always
    ports:
      - "8181:8181" 
    environment:
      - TZ=${TZ:-Europe/Lisbon}
      - NIFI_WEB_HTTP_PORT=8181
    volumes:
      - ./nifi/conf:/opt/nifi/nifi-current/conf
      - ./nifi/lib/postgresql-42.7.1.jar:/opt/nifi/nifi-current/lib/postgresql-42.7.1.jar

  data_synthesizer:
    image: ghcr.io/forcera/data_synthesizer:latest
    environment:
      - TZ=${TZ:-Europe/Lisbon}
      - mosquitto_username=${mosquitto_username:-default_admin_user}
      - mosquitto_password=${mosquitto_password:-default_admin_password}
      - POSTGRES_PASSWORD=${POSTGRES_PASSWORD:-default_admin_password}
      - POSTGRES_USER=${POSTGRES_USER:-default_admin_user}
      - POSTGRES_DB=${POSTGRES_DB:-default_database}
    depends_on:
      - mosquitto
      - timescaledb

volumes:
  ditto_log_files:
    driver: local
    driver_opts:
      type: none
      device: /var/log/ditto
      o: bind,uid=1000,gid=1000

  config:
  data:
  log:
