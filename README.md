# OfftwAIn

OfftwAIn, financed under the [H2020 GreenOffshoreTech](https://greenoffshoretech.com/) project, pioneers advancements in offshore solar energy operations. 

Through technologies like Digital Twin, Data Analytics, Predictive Maintenance, and the Internet of Things (IoT), OfftwAIn aims to revolutionize maintenance practices for offshore solar installations. By providing real-time monitoring and predictive maintenance capabilities, OfftwAIn reduces maintenance expenses while enhancing operational efficiency. Additionally, it has the potential to positively impact the environment by optimizing energy production and mitigating the effects of corrosion and marine organisms.

## Overview
This project leverages Docker Compose to orchestrate a multi-service application including MongoDB, Eclipse Ditto, Mosquitto MQTT broker, TimescaleDB, Grafana, and Apache NiFi.

## Open-source Components
- **Eclipse Ditto**: Digital twin framework.
- **Mosquitto**: MQTT broker for messaging.
- **TimescaleDB**: Time-series SQL database.
- **Grafana**: Analytics and monitoring solution.
- **Apache NiFi**: Data flow automation tool.

## Workflow

### ditto
Eclipse Ditto is an open-source framework for building and managing digital twins in the Internet of Things (IoT). Digital twins are virtual representations of physical devices, enabling interaction and management through web APIs.

### ditto_starting
This service updates policies and things in Ditto using curl commands, modifies connection configurations for MQTT brokers, and initializes IoT device's (solar panel's) attributes and features in Ditto. The script uses subprocesses to execute these curl commands and handling JSON data for configurations.

### wheather_data_docker
This service fetches weather forecast data from Tomorrow.io's API, extracts specific weather parameters (temperature, humidity, dew point, wind speed, wind direction, and cloud cover), and then publishes this data as a JSON message to an MQTT topic (weather.sensor/01) using the Paho MQTT client. It utilizes environment variables for MQTT authentication (mosquitto_username and mosquitto_password). The script loops indefinitely, requesting and publishing new weather data every 3 minutes.

### ui
This service composes the Digital Twin visualization layer. It heavily relies on Three.js for its web-based 3D visualization compoment. 

## Additional Resources
- [Docker Compose Documentation](https://docs.docker.com/compose/)
- [Eclipse Ditto Documentation](https://www.eclipse.org/ditto/)
- [Grafana Documentation](https://grafana.com/docs/)
