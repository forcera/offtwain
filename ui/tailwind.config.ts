import type { Config } from "tailwindcss";

const config: Config = {
  content: [
    "./src/pages/**/*.{js,ts,jsx,tsx,mdx}",
    "./src/components/**/*.{js,ts,jsx,tsx,mdx}",
    "./src/app/**/*.{js,ts,jsx,tsx,mdx}",
  ],
  theme: {
    extend: {
      colors: {
        blackout: '#161C17',  
        lime: '#E3EDD0',
        flame: '#F96A40',
        flake: '#E6E4DE',
        cooper: '#EFDEBB',  // Adding custom blue color
        secundaryBlue: '#99ccd7',  // Adding custom light blue color
        primaryBlue: '#2a2b78'  // Adding custom blue color
      },
    },
  },
  plugins: [],
};

export default config;