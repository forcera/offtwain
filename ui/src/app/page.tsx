import Navbar from "./components/nav/Navbar";
import SolarModel from "./components/model/SolarModel";

export default function Home() {
  return (
    <main className="h-screen overflow-hidden">
      <Navbar />
      <SolarModel />
    </main>
  );
}
