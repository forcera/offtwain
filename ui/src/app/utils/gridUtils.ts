export function determineGridSize(totalElements: number) {
  const numRows = 3; // Fixed number of rows
  if (totalElements < 1) return { rows: 0, columns: 0 }; // Return 0 if no elements

  let columns = Math.ceil(totalElements / numRows);

  return { rows: numRows, columns: columns };
}