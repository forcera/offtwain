// Function used for video-recording and product demonstration purposes. To be discarded in nominal usage. 
export function randomizeMeasurements(data: any[]) {
  const randomAdjustment = (value: number) => {
      const maxDeviation = value * 0.05; // Calculate 5% of the current value
      return value + (Math.random() * (maxDeviation * 2) - maxDeviation); // Generate a number between -maxDeviation and +maxDeviation
  };

  return data.map(item => {
      if (item.features && item.features.measurements) {
          Object.keys(item.features.measurements.properties).forEach(key => {
              item.features.measurements.properties[key] = randomAdjustment(item.features.measurements.properties[key]);
          });
          item.features.measurements.properties.status = "Normal";

          if (item.thingId === "offtwAIn_panel:F1-S0-P4") {
              item.features.measurements.properties.status = "Fault - Degradation";
              item.features.measurements.properties['p_dc'] *= 0.55;
          }
          if (item.thingId === "offtwAIn_panel:F1-S1-P5") {
              item.features.measurements.properties.status = "Fault - Short Circuit";
              item.features.measurements.properties['p_dc'] = 0;
              item.features.measurements.properties['v_dc'] = 0;
              item.features.measurements.properties['i_dc'] = 0;
          }
      }
      return item;
  });
}