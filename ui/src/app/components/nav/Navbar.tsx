import Link from 'next/link';

const Navbar = () => {
    return (
        <nav className="bg-blackout text-lime p-4">
            <div className="container mx-auto flex justify-between items-center">
                <div className="flex">
                    <a href="https://forcera.pt/" target="_blank" rel="noopener noreferrer">
                    <img src="/FORCERA-ICON.svg" alt="Forcera icon" className="h-8 mr-2" />
                    </a>
                <Link href="/" className="mr-4 font-extrabold text-2xl">
                    OfftwAIn
                </Link>
                </div>
                <div>
                    <a 
                        href="http://localhost:3000/d/c75c6503-5eff-48a3-b5cd-7dc4759ea18c/weather-dashboard?orgId=1&from=now-6h&to=now"               className="mr-12 font-bold text-lg"
                        target="_blank"
                        rel="noopener noreferrer">
                            Weather Data
                    </a>
                    <a 
                        href="http://localhost:3000/d/c241251a-5b27-4080-a794-f035f78b00d1/fpv-inclination?orgId=1" className="font-bold text-lg"
                        target="_blank"
                        rel="noopener noreferrer">
                            FPV Dashboard
                    </a>
                </div>
            </div>
        </nav>
    )
}

export default Navbar;