import React from 'react';

interface LocationProps {
    data: any; // Define more specific type if possible
}

const Location: React.FC<LocationProps> = ({ data }) => {
    if (!data) return null; // Ensures that data is available before rendering

    const { latitude, longitude } = data.features.measurements.properties;

    return (
        <div className="absolute top-20 right-[80px] text-blackout font-semibold rounded-lg break-all">
            <div className="text-sm">
                <p>Latitude: {latitude.toFixed(4)}</p>
                <p>Longitude: {longitude.toFixed(4)}</p>
            </div>
        </div>
    );
};

export default Location;