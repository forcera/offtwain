import React from 'react';

interface WeatherDataProps {
    data: any; // Define more specific type if possible
}

const WeatherData: React.FC<WeatherDataProps> = ({ data }) => {
    if (!data) return null; // Ensure data is available before rendering

    const { temperature, humidity, dewPoint, windSpeed, windDirection, cloudCover } = data.features.measurements.properties;

    return (
        <div className="absolute w-80 bottom-5 left-5 text-white font-bold pt-4 rounded-lg break-all">
            <div className="p-4 text-base">
                <p>Temperature: {temperature.toFixed(1)} °C</p>
                <p>Humidity: {humidity.toFixed(1)} %</p>
                <p>Dew Point: {dewPoint.toFixed(1)} °C</p>
                <p>Wind Speed: {windSpeed.toFixed(1)} m/s</p>
                <p>Wind Direction: {windDirection.toFixed(1)}°</p>
                <p>Cloud Cover: {cloudCover} %</p>
            </div>
        </div>
    );
};

export default WeatherData;