import * as THREE from 'three'
import React, { useMemo, useRef } from 'react'
import { extend, useFrame, useLoader } from '@react-three/fiber'
import { Water } from 'three-stdlib'

extend({ Water })

const Ocean = () => {
    const ref: any = useRef<THREE.Mesh>(null!);
    const waterNormals = useLoader(THREE.TextureLoader, '/waterNormals.jpg');
    waterNormals.wrapS = waterNormals.wrapT = THREE.RepeatWrapping;
    const waterGeom = useMemo(() => new THREE.PlaneGeometry(10000, 10000), []);
    const config = useMemo(() => ({
        textureWidth: 512,
        textureHeight: 512,
        waterNormals,
        sunDirection: new THREE.Vector3(),
        sunColor: 0xffffff,
        waterColor: 0x001e0f,
        distortionScale: 3.7,
        fog: false,
    }), [waterNormals]);

    useFrame((state, delta) => (ref.current.material.uniforms.time.value += delta));

    return <water ref={ref} args={[waterGeom, config]} rotation-x={-Math.PI / 2} />
}

export default Ocean;
