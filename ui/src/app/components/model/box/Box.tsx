import * as THREE from 'three';
import React, { useRef } from 'react';

// Define the props interface
interface BoxProps {
  boxWidth: number;
  cloneHeight: number;
  boxDepth: number;
  margin: number;
}

const Box = ({ boxWidth, cloneHeight, boxDepth, margin }: BoxProps) => {
    const ref = useRef<THREE.Mesh>(null);

    return (
        <mesh ref={ref}>
            <boxGeometry args={[
                boxWidth + margin * 2,
                cloneHeight / 2 + 2,
                boxDepth + margin * 2,
            ]} />
            <meshBasicMaterial color={'#ffffff'}/>
        </mesh>
    )
}

export default Box;