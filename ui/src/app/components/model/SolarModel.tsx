"use client"

import axios from 'axios';

import { useEffect } from 'react';
import { useState, useRef, MutableRefObject } from "react"
import { Canvas, useLoader } from '@react-three/fiber'
import { Sky, OrbitControls } from "@react-three/drei"
import { Mesh, MeshStandardMaterial } from 'three'
import { Preload } from '@react-three/drei'
import { r3f } from '../../helpers/global'
import Box from './box/Box'
import Ocean from './ocean/Ocean'
import WeatherData from './weatherdata/WeatherData';
import Location from './location/Location';

import { determineGridSize } from '../../utils/gridUtils';
import { randomizeMeasurements } from '../../utils/measurementUtils';



const loader = require('three/examples/jsm/loaders/GLTFLoader');

export default function SolarModel() {
    const [NUM_COLS, setNumCols] = useState(0);
    const [NUM_ROWS, setNumRows] = useState(0);
    const [selectedClone, setSelectedClone] = useState<string | null>(null);
    const [apiData, setApiData] = useState([]);
    const [blink, setBlink] = useState(false);

    // Clone specific data
    const CLONE_SPACING = 5 // space between each clone
    const CLONE_WIDTH = 10 // width of each clone
    const CLONE_HEIGHT = 2.5 // height of each clone
    const CLONE_DEPTH = 10 // depth of each clone

    // Box platform specific data
    const BOX_WIDTH = NUM_COLS * (CLONE_WIDTH + CLONE_SPACING)
    const BOX_DEPTH = NUM_ROWS * (CLONE_DEPTH + CLONE_SPACING)
    const PLATFORM_CELL_WIDTH = BOX_WIDTH / NUM_COLS
    const PLATFORM_CELL_DEPTH = BOX_DEPTH / NUM_ROWS
    const MARGIN = 2
    const maxScaleFactor = Math.min(PLATFORM_CELL_WIDTH / CLONE_WIDTH, PLATFORM_CELL_DEPTH, CLONE_DEPTH)


    useEffect(() => {
        fetchDataFromDitto(); // Call immediately on component mount
    
        const intervalId = setInterval(() => {
            fetchDataFromDitto(); // Call every 3 minutes
        }, 15000); // 180000 milliseconds = 3 minutes

        // Set up interval for blinking every 1,2 seconds
        const blinkInterval = setInterval(() => {
            setBlink(blink => !blink);
        }, 1200); // 1500 milliseconds = 1,2 seconds
    
        return () => {
            clearInterval(intervalId);
            clearInterval(blinkInterval); 
        }
    }, []);

  
    const fetchDataFromDitto = async () => {
        const config = {
            auth: {
                username: 'ditto',
                password: 'ditto'
            }
        };
        try {
            const response = await axios.get('http://localhost:8080/api/2/things', config);
            const data = randomizeMeasurements(response.data)
            setApiData(data);
            console.log(response.data)
            console.log('Number of elements:', response.data.length);
            const totalElements = response.data.length - 1
            const gridSize = determineGridSize(totalElements)
            setNumCols(gridSize['columns'])
            setNumRows(gridSize['rows'])
        } catch (error) {
            console.error('Error fetching data from Ditto API:', error);
        }
    };


    const handleClick = (panelId: string) => {
        setSelectedClone(panelId === selectedClone ? null : panelId);
    };


    const Clone = () => {
        const mesh: MutableRefObject<any> = useRef<Mesh>(null!);
        const gltf: any = useLoader(loader.GLTFLoader, '/solarPanel/scene.gltf');
    
        let panels = [];
    
        // Slice apiData to exclude the last element
        const effectiveData = apiData.slice(0, -1); // Slice off the last element
    
        effectiveData.forEach((thing: any, index: number) => {
            const cloneId = thing.thingId; // Using thingId from apiData
            let clone = gltf.scene.clone();
            clone.name = cloneId;

            if(thing.features.measurements.properties.status !== "Normal" && blink) {
                clone.traverse((child) => {
                    if (child.isMesh) {
                        child.material = new MeshStandardMaterial({ color: '#FF0000' });  // You can dynamically set this color
                    }
                });
            }

    
            // Calculating position based on index if rows and columns are not defined
            const col = index % NUM_COLS;
            const row = Math.floor(index / NUM_COLS);
    
            const position = [
                -BOX_WIDTH / 2 + col * PLATFORM_CELL_WIDTH + PLATFORM_CELL_WIDTH / 2,
                CLONE_HEIGHT / 2,
                -BOX_DEPTH / 2 + row * PLATFORM_CELL_DEPTH + PLATFORM_CELL_DEPTH / 2
            ] as const;
            const scaleFactor = maxScaleFactor * 1.625;
            clone.scale.set(scaleFactor, scaleFactor, scaleFactor);
        
    
            panels.push(
                <mesh key={cloneId} onClick={() => handleClick(cloneId)} ref={mesh} position={position}>
                    <primitive object={clone} scale={[scaleFactor, scaleFactor, scaleFactor]} />
                </mesh>
            );
        });
    
        return <>{panels}</>;
    };
    

    const TextBox = ({ panelId, data }: { panelId: string; data: any }) => {
        // Replace 'offtwAIn_panel' with 'Panel' in the panelId
        const displayPanelId = panelId.replace("offtwAIn_panel:", "Panel ");

        if (selectedClone === panelId) {
            return (
                <div className="absolute w-80 top-24 left-10 bg-blackout text-white pt-4 rounded-lg break-all">
                    <p className="flex justify-center items-center text-lime pb-2 font-bold text-xl">{displayPanelId}</p>
                    <div className="p-4 font-bold text-medium">
                        <p>Status: <span className={data.features.measurements.properties.status !== "Normal" ? "font-bold text-red-600" : "font-bold text-lime"}>{data.features.measurements.properties.status}</span></p>
                        <p>DC Power: <span className="font-bold text-lime">{data.features.measurements.properties.p_dc.toFixed(2)} W</span></p>
                        <p>DC Current: <span className="font-medium text-lime">{data.features.measurements.properties.i_dc.toFixed(3)} A </span></p>
                        <p>DC Voltage: <span className="font-medium text-lime">{data.features.measurements.properties.v_dc.toFixed(2)} V</span></p>
                    </div>
                    <div className="p-4 font-bold text-medium">
                        <p>Inclination: <span className="font-medium text-lime">{data.features.measurements.properties.inclination.toFixed(2)}</span></p>
                        <p>Irradiance: <span className="font-medium text-lime">{data.features.measurements.properties.irradiance.toFixed(2)} kWh/m²</span></p>
                    </div>
                </div>
            );
        }
        return null;
    };


    return (
        <div className='flex justify-center items-center h-screen'>
            <Canvas className='h-2xl w-2xl' camera={{ position: [-10, 55, 95], fov: 55, near: 1, far: 20000 }}>
                <OrbitControls />
                <ambientLight />
                <pointLight position={[10, 10, 10]} />
                <Clone />
                <Box 
                    boxWidth={BOX_WIDTH} 
                    cloneHeight={CLONE_HEIGHT} 
                    boxDepth={BOX_DEPTH} 
                    margin={MARGIN}
                />
                <Ocean />
                <Sky />
                <r3f.Out />
                <Preload all />
            </Canvas>
            {apiData && apiData.slice(0, -1).map((thing, index) => (
                <TextBox key={thing.thingId} panelId={thing.thingId} data={thing} />
            ))}
            {apiData && apiData.length > 0 && (
                <WeatherData data={apiData[apiData.length - 1]} />
            )}
            {apiData && apiData.length > 0 && (
                <Location data={apiData[apiData.length - 1]} />
            )}
            <a href="https://forcera.pt/" target="_blank" rel="noopener noreferrer">
            <img className="absolute bottom-4 right-[50px] h-16" src='/FORCERA-LOGO-02.svg' />
            </a>
        </div>
    )
}
